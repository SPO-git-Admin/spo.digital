/*** mmenu_navigation ***/
Mmenu.options.slidingSubmenus = true;
//Mmenu.configs.classNames.selected = "active";
Mmenu.configs.offCanvas.page.selector = "#my-page";

document.addEventListener(
    "DOMContentLoaded", () => {
        const menu = new Mmenu( "#my-menu", {
            extensions: {
                "all": [
                    "position-right",
                    "border-none",
                    "pagedim-black"
                ],
                //"(min-width: 400px)": ["fullscreen"]
            },
            "navbar": {
                "add": true,
            },
            "navbars": [
                {
                    "position": "bottom",
                    "use": true,
                    "content": [
                        "<div class='container'>" +
                        "<div class='row'>" +
                        "<div class='col-24'>" +
                            "<div class=\"mainnav__partner ce_rsce_master_header_partners\">\n" +
                        "<ul class=\"mainnav__partner-list d-flex justify-content-end align-items-center list-unstyled\">\n" +
                        "<li class=\"mainnav__partner-item\">\n" +
                        "<a href=\"https://www.shopware.com/de/\" aria-label=\"Link zu Shopware.com\">\n" +
                        "<img class=\"mainnav__partner-icon\" src=\"files/S-Punkt-Online/images/icons/shopware-business-partner.png\" alt=\"Shopware Business Partner\" width=\"350\" height=\"500\">\n" +
                        "</a>\n" +
                        "</li>\n" +
                        "<li class=\"mainnav__partner-item\">\n" +
                        "<a href=\"https://contao.org/de/\" aria-label=\"Link zu Contao.org\">\n" +
                        "<img class=\"mainnav__partner-icon\" src=\"files/S-Punkt-Online/images/icons/contao_official-partner.svg\" alt=\"Contao official Partner\" width=\"90\" height=\"130\">\n" +
                        "</a>\n" +
                        "</li>\n" +
                        "</ul>\n" +
                        "</div>"+
                        "</div>" +
                        "<div class='col-24'>" +

                        "<ul class='mmenu__footer-icon-list list-unstyled pt-2 d-flex" +
                        " justify-content-between'>" +
                        "<li class='mmenu__footer-icon-item'>" +
                        "<a href='https://www.facebook.com/spunktonline/' title='Zum Facebook-Profil von S Punkt Online' class='mmenu__footeer-icon-link'>" +
                        "<img alt='Facebook Logo' src='/files/S-Punkt-Online/images/svg/FB.svg'>" +
                        "</a>" +
                        "</li>" +
                        "<li class='mmenu__footer-icon-item'>" +
                        "<a href='https://g.page/spunktonline/' title='Zum Google-Profil von S Punkt Online' class='mmenu__footeer-icon-link'>" +
                        "<img alt='Google My Business Logo' src='/files/S-Punkt-Online/images/svg/google.svg'>" +
                        "</a>" +
                        "</li>" +
                        "<li class='mmenu__footer-icon-item'>" +
                        "<a href='https://www.instagram.com/spunktonline/' title='Zum Instagram-Profil von S Punkt Online' class='mmenu__footeer-icon-link'>" +
                        "<img alt='Instagram Logo' src='/files/S-Punkt-Online/images/svg/instagram.svg'>" +
                        "</a>" +
                        "</li>" +
                        "<li class='mmenu__footer-icon-item'>" +
                        "<a href='https://www.linkedin.com/company/s-punkt-online' title='Zum LinkedIn-Profil von S Punkt Online'" +
                        " class='mmenu__footeer-icon-link'>" +
                        "<img alt='LinkedIn Logo' src='/files/S-Punkt-Online/images/svg/linkedIn.svg'>" +
                        "</a>" +
                        "</li>" +
                        "<li class='mmenu__footer-icon-item'>" +
                        "<a href='https://www.xing.com/pages/spunktonline' title='Zum XING-Profil von S Punkt Online' class='mmenu__footeer-icon-link'>" +
                        "<img alt='XING Logo' src='/files/S-Punkt-Online/images/svg/xing.svg'>" +
                        "</a>" +
                        "</li>" +
                        "</ul>" +
                        "</div>" +
                        "</div>" +
                        "</div>"
                    ]
                }
            ],
            "iconbar": {
                "use": false,
                "position": "bottom",
                "content": [
                    "<a href='tel:+4917671964147'><img src='/files/S-Punkt-Online/images/icons/social/phone-logo.svg'" +
                    " width='20' height='20' alt='telefon-icon'></a>",
                    "<a href='whatsapp://send?text=Hallo!&phone=+4917671964147 &=+4917671964147'><img src='images/icons/social/whatsapp-logo.svg' width='20' height='20' alt='whatsapp-icon'</a>",
                    "<a href='tg://resolve?domain=@rshdbairamov'><img src='images/icons/social/telegram-logo.svg' width='20' height='20' alt='telegram-icon'</a>",
                    "<a href='mailto:direct@rshdbairamov.com?subject=Hallo!'><img src='images/icons/social/mail-logo.svg' width='20' height='20' alt='mail-icon'</a>",
                    "<a href='https://www.instagram.com/herrbairamov/'><img src='images/icons/social/instagram-logo.svg' width='20' height='20' alt='instagram-icon'</a>",
                    "<a href='https://m.facebook.com/rashid.bairamov.96'><img src='images/icons/social/facebook-logo.svg' width='20' height='20' alt='facebook-icon'</a>"
                ]
            }
        } );

    }
);


$(document).ready(function (){
    $('.mmenu__open-button').click(function(){
        $('.header__content-container').fadeIn();
    });
    $('.mm-wrapper__blocker').click(function(){
        $('.header__content-container').fadeOut()
    });
    $('.mmenu__close-button').click(function () {
        $('.header__content-container').fadeOut(100)
    })
});


/*** header_navigation_shadow ***/
$(window).scroll((function() {
    $('.mainnav').toggleClass('header-shadow mainnav--scroll', $(this).scrollTop() > 0);

    let scroll = $(window).scrollTop();
    if (scroll >= 1 && window.innerWidth >= 1024) {
        $(".header__contacts").slideUp();
    } else if (scroll === 0 ){
        $(".header__contacts").slideDown();
    }
}));


/*** slider ***/
let swiper = new Swiper('.swiper-container', {
    // Optional parameters
    //loop: true

    autoplay: {
        delay: 5000,
    },

    // If we need pagination
    pagination: {
        el: '.swiper-pagination', clickable: true,
    },

    // Navigation arrows
    navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
    },

    // And if we need scrollbar
},);

let swiperNews = new Swiper('.swiper-container__news', {
    breakpoints: {
        0: {
            direction: 'vertical',
            slidesPerView: 1,
        },
        768: {
            direction: 'horizontal',
            slidesPerView: 2,
            spaceBetween: 10,
        },
        1024: {
            direction: 'horizontal',
            slidesPerView: 3,
            spaceBetween: 10,
        },
        1280: {
            direction: 'horizontal',
            slidesPerView: 3,
            spaceBetween: 20,
        }
    },
    navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
    },
},);



/*** subpages_gallery ***/
$(document).ready(function () {
    $('.subpages-gallery__wrapper').first().addClass('active');


    $('.subpages-gallery__wrapper').not($(this).first()).hover(function () {
        $('.subpages-gallery__wrapper').first().removeClass('active');
        $(this).toggleClass('active__ms');

    });


    $('.subpages-gallery__item').hover(function () {
        $(this).children('.subpages-gallery__container').fadeToggle();

    });
});



/*** subpages_services_widget ***/
$(document).ready(function() {

    $('.subpages-services__item-container .subpages-services__item').on('click', f_acc);

    function f_acc() {
        $(this).toggleClass('active');
        $('.subpages-services__item-container .subpages-services__item-description').not($(this).next()).slideUp(500);
        $('.subpages-services__item-container .subpages-services__item').not($(this)).removeClass('active');
        $(this).next().slideToggle(500);
    }

});

/**** team ***/
$(".team__item-button").click( function () {
    //$(".team__item").children(".team__item-description-container").hide();
    $(this).toggleClass("active").next(".team__item-description-container").fadeToggle();
    $(this).prevAll(".team__item-image").fadeToggle();
});





/*** navigation_desktop_header ***/
$(document).ready( function () {

    $('ul.level_1.mmenu__list li.mmenu__item').hover(function () {

        clearTimeout($.data(this,'timer'));


        $('ul.level_2.mmenu__list',this).stop(true,true).addClass('mainnav--display');

    }, function () {

        $.data(this,'timer', setTimeout($.proxy(function() {
            $('ul.level_2.mmenu__list',this).stop(true,true).removeClass('mainnav--display');

        }, this), 450));

    });

    let agentur = $('li.agentur-page ul.level_2');
    $(agentur).prepend('<li class="mainnav__title">Agentur</li>');

    let aktuelles = $('li.aktuelles ul.level_2');
    $(aktuelles).prepend('<li class="mainnav__title">Aktuelles</li>');

    let webentwicklung = $('li.webentwicklung ul.level_2');
    $(webentwicklung).prepend('<li class="mainnav__title">Webentwicklung</li>');

    let ecommerce = $('li.e-commerce ul.level_2');
    $(ecommerce).prepend('<li class="mainnav__title">E-Commerce</li>');

    $('a.planung-konzeption').removeAttr('href');
    $('a.analyse').removeAttr('href');
    $('a.umsetzung').removeAttr('href');


});

/***************
 * FORM TEASER *
 ***************/
$(document).ready( function () {
    $('.form-teaser__button').click(function () {
        $('.form-teaser__form').slideToggle(300);
    });
});



/**************************
 * SOCIAL STICKY CONTACTS *
 **************************/
$(document).ready(function () {
    let offset;
    $(function(){
        offset=$('.social_sticky').offset().top
    });

    $(window).scroll(function(){
        let top= $(window).scrollTop()+200;
        $('.social_sticky').animate({top:top},0)
    });

    jQuery.fn.slideLeftHide = function( speed, callback ) {
        this.animate({
            width: "hide",
            marginLeft: "hide",
            marginRight: "hide"
        }, speed, callback );
    }

    jQuery.fn.slideLeftShow = function( speed, callback ) {
        this.animate( {
            width: "show",
            marginLeft: "show",
            marginRight: "show"
        }, speed, callback );
    }

    $(".button").on('click', function() {
        let socialStickyContacts = $(".social_sticky__container");
        if (socialStickyContacts.is(':visible')) {
            socialStickyContacts.fadeOut(10);
        }else{
            socialStickyContacts.fadeIn(10);
        }
    });

    $("#act_social_sticky__container_close").on('click', function() {
        let socialStickyContacts = $(".social_sticky__container");
        if (socialStickyContacts.is(':visible')) {
            socialStickyContacts.fadeOut(10);
        }
    });

    $(document).mouseup(function (e){
        let socialStickyContacts = $(".social_sticky__container");
        if (!socialStickyContacts.is(e.target)
            && socialStickyContacts.has(e.target).length === 0) {
            socialStickyContacts.fadeOut(10);
        }
    });
});


/********************
 * LIST OF FEATURES *
 ********************/
$(document).ready(function (){
    let text_container_hidden = $('.rsce_lage__text_container--hidden');
    let act_text_container_show = $('button.rsce_lage__button')
    act_text_container_show.click( function () {
        $(this).fadeOut(100);
        text_container_hidden.slideDown(300);
    });
});



/********
 * JOB *
 *******/

$(document).ready(function (){
    let job_container = $(".job__container");
    let act_job_container_show = $(".job__title_container");

    act_job_container_show.click(function () {
        $(this).next(job_container).slideToggle();
        $(this).toggleClass("job_show");
    });
});


/************************
 * SCROLL TO TOP BUTTON *
 ***********************/

let scroll_to_top_button = $('#scroll_to_top');

$(window).scroll(function() {
    if ($(window).scrollTop() > 650) {
        scroll_to_top_button.addClass('show');
    } else {
        scroll_to_top_button.removeClass('show');
    }
});

scroll_to_top_button.on('click', function(e) {
    e.preventDefault();
    $('html, body').animate({scrollTop:0}, '100');
});



/********
 * JOB *
 *******/

$(document).ready(function (){
    let services_button = $( '.services__form_button' );
    let services_form = $( '.services__form_container' )
    services_button.click( function () {
       $(this).next(services_form.slideDown());
    });
});


// === WORKSHOPS-FORM === //

let act_to_employ = $(".rsce_detmold a.services__form_button");
let inp_employment_subject = $(".rsce_detmold input.workshop_form__subject");

$(act_to_employ).click( function () {
    let title = $(this).parent().parent().find("h3.services__card-title");


    let offset = 200;
    $(".services__form_container").fadeIn();
    $('html, body').animate({
        scrollTop: $(".services__form_container").offset().top - offset
    }, 1000);


    $(inp_employment_subject).val('');
    $(inp_employment_subject).val($(title).text());
});





// === job form === //

/*** Employment Button ***/
let job_act_button = $("a.job__button");
let job_subjekt = $("input.job_form__subject");

$(job_act_button).click( function () {
    let offset = 100;
    $(".job_form").fadeIn();
    $('html, body').animate({
        scrollTop: $(".job_form").offset().top - offset
    }, 1000);

    let job_title = $(".job__title");
    $(job_subjekt).val('');
    $(job_subjekt).val($(job_title).text());
});

/* profile page */

let profile_page_question_button = $( ".rsce_berlin__questions_item_title_container" );
let profile_page_answer_container = $( ".rsce_berlin__questions_item_text" );

$( profile_page_question_button ).click( function () {
    if ( window.innerWidth <= 1200 ) {
        $(this).next(profile_page_answer_container).slideToggle();
    }
});


/* gallery */
let magicGrid = new MagicGrid({
    container: '.rsce_gallery__container',
    animate: true,
    gutter: 10,
    static: true,
    useMin: true
});

magicGrid.listen();





