<?php
#####################################
### Created by S Punkt Online #######
### https://www.s-punkt-online.de ###
#####################################

return array(
    'label' => array(
        'de' => array('Footer: Kontakte und Anschrift', ''),
    ),

    'types' => array('content'),
    'contentCategory' => 'RSCE: NODE',
    //'beTemplate' => 'be_wildcard',
    'standardFields' => array('cssID'),
    'wrapper' => array(
        'type' => 'none',
    ),

    'fields' => array(
        'company_name' => array(
            'label' => array('Firmenname', 'Hier können Sie der Firmenname hinzufügen'),
            'inputType' => 'text',
            'eval' => array('mandatory'=>true, 'tl_class'=>'w50 clr'),
        ),
        'address' => array(
            'label' => array('Anschrift', 'Hier können Sie die Anschrift hinzufügen'),
            'inputType' => 'text',
            'eval' => array('mandatory'=>true, 'tl_class'=>'w50'),
        ),
        'tel' =>  array(
            'label' => array('Telefonnummer', 'Hier können Sie die Telefonnummer hinzufügen'),
            'inputType' => 'text',
            'eval' => array('mandatory'=>true, 'tl_class'=>'w50 clr'),
        ),
        'work_time' => array(
            'label' => array('Öffnungszeit', 'Hier können Sie die Öffnungszeiten hinzufügen'),
            'inputType' => 'text',
            'eval' => array('mandatory'=>true, 'tl_class'=>'w50'),
        ),
        'mail' => array(
            'label' => array('E-Mail', 'Hier können Sie die E-Mail hinzufügen'),
            'inputType' => 'text',
            'eval' => array('mandatory'=>true, 'tl_class'=>'w50 clr'),
        ),
        'mail_text' => array(
            'label' => array('E-Mail Text', 'Hier können Sie den Text für die E-Mail hinzufügen'),
            'inputType' => 'text',
            'eval' => array('mandatory'=>true, 'tl_class'=>'w50'),
        ),
    ),
);