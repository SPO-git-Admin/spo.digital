<?php

#####################################
### Created by S Punkt Online #######
### https://www.s-punkt-online.de ###
#####################################

return array(
    'label' => array(
        'Kooperationen-Liste: Logo/Avatar, Name, Position, Beschreibung',
    ),
    'types' => array('content'),
    'contentCategory' => 'RSCE: Inhalt',
    //'beTemplate' => 'be_wildcard',
    'standardFields' => array('cssID'),
    'wrapper' => array(
        'type' => 'none',
    ),
    
    'fields' => array(
        'list' => array(
            'label' => array('Kooperationspartner', 'Fügen Sie Elemente hinzu.'),
            'elementLabel' => '%s. Item',
            'inputType' => 'list',
            'minItems' => 1,
            'fields' => array(
                /*** TEXT ***/
                'title' =>  array(
                    'label' => array(
                        'de' => array('Vorname und Nachname', 'Hier können Sie Vorname und Nachname hinzufügen'),
                    ),
                    'inputType' => 'text',
                    'eval' => array('tl_class' => 'w50 clr'),
                ),
                /*** TEXT ***/
                'role' =>  array(
                    'label' => array(
                        'de' => array('Position', 'Hier können Sie die Position hinzufügen'),
                    ),
                    'inputType' => 'text',
                    'eval' => array('tl_class' => 'w50 clr'),
                ),
                /*** IMAGE ***/
                'avatar' => array(
                    'label' => array(
                        'de' => array('Avatar', 'Hier können Sie das Bild auswählen'),
                    ),
                    'inputType' => 'fileTree',
                    'eval' => array(
                        'fieldType' => 'radio',
                        'filesOnly' => true,
                        'extensions' => 'jpg,jpeg,png,gif,svg',
                        'tl_class'=>'w50 clr',
                    ),
                ),
                'alt' =>  array(
                    'label' => array(
                        'de' => array('Alt-Attribute', 'Hier können Sie Alt-Attribute hinzufügen'),
                    ),
                    'inputType' => 'text',
                    'eval' => array(
                        'tl_class' => 'w50 clr',
                        'mandatory' => true,
                    ),
                ),
                /*** TEXTAREA ***/
                'text' =>  array(
                    'label' => array(
                        'de' => array('Beschreibung', 'Hier können Sie eine Beschreibung hinzufügen'),
                    ),
                    'inputType' => 'textarea',
                    'eval' => array('rte' => 'tinyMCE', 'tl_class' => 'clr'),
                ),
            ),
        ),
    ),
);