<?php
return array(
    'label' => array(
        'de' => array('Header: Desktop-Logo', ''),
    ),

    'types' => array('content'),
    'contentCategory' => 'RSCE: NODE',
    //'beTemplate' => 'be_wildcard',
    'standardFields' => array('cssID'),
    'wrapper' => array(
        'type' => 'none',
    ),
    

    'fields' => array(
        // GROUP
        'logo_grp' => array(
            'label' => array('Logo-Gruppe', 'Fügen Sie ein Bild ein.'),
            'inputType' => 'group',
        ),

        'logo' => array(
            'label' => array(
                'de' => array('Logo', 'Hier können Sie das Bild auswählen'),
            ),
            'inputType' => 'fileTree',
            'eval' => array(
                'fieldType' => 'radio',
                'filesOnly' => true,
                'extensions' => 'jpg,jpeg,png,gif,svg',
                'tl_class'=>'w50',
                'mandatory'=>true,
            ),
        ),
        'alt' => array(
            'label' => array(
                'de' => array('Alt-Attribute', 'Hier können Sie den Text für Alt-Attribute hinzufügen'),
            ),
            'inputType' => 'text',
            'eval' => array('tl_class' => 'w50 clr', 'mandatory'=>true),
        ),

        // TEXT
        'logo_title' => array(
            'label' => array(
                'de' => array('Bildtitel', 'Hier können Sie den Titel des Bildes eingeben (title-Attribut)'),
            ),
            'inputType' => 'text',
            'eval' => array('tl_class' => 'w50'),
        ),

        // GROUP
        'button_grp' => array(
            'label' => array('Link-Gruppe', 'Fügen Sie einen Link ein.'),
            'inputType' => 'group',
        ),

        'link' => array(
            'label' => array(
                'de' => array('Link', 'Hier können Sie ein Link auswählen'),
            ),
            'inputType' => 'url',
            'eval' => array('tl_class' => 'w50 clr', 'mandatory'=>true),

        ),

        // TEXT //
        'link_title' => array(
            'label' => array(
                'de' => array('Link-Title', 'Hier können Sie einen Titel für den Link eingeben'),
            ),
            'inputType' => 'text',
            'eval' => array('tl_class' => 'w50 clr'),
            'dependsOn' => [
                'field' => 'checkbox_button',
            ],
        ),
    ),
);