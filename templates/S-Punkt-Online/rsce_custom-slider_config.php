<?php

return array(
    'label' => array(
        'de' => array('Slider: Titel, Icon und eine Beschreibung', ''),
    ),
    'types' => array('content'),
    'contentCategory' => 'RSCE: Inhalt',
    //'beTemplate' => 'be_wildcard',
    'standardFields' => array('headline', 'cssID'),
    'wrapper' => array(
        'type' => 'none',
    ),

    'fields' => array(
        
        'sliderList' => array(
            'label' => array(
                'de' => array('Sliderliste ', 'Hier können Sie Slider-Elemente ergänzen'),
            ),
            'elementLabel' => array(
                'de' => 'Slider-Item %s',
            ),

            'inputType' => 'list',
            'fields' => array(
                'sliderItemIcon' => array(
                    'label' => array(
                        'de' => array('Icon', 'Hier können Sie ein Icon für das Slider-Element auswählen'),
                    ),
                    'inputType' => 'fileTree',
                    'eval' => array(
                        'fieldType' => 'radio',
                        'filesOnly' => true,
                        'extensions' => 'jpg,jpeg,png,gif,svg',
                    ),
                ),
                // TEXT
                'sliderItemIcon_alt' => array(
                    'label' => array(
                        'de' => array('Alt-Attribute', 'Hier können Sie einen Alt-Attribute für das Bild eingeben'),
                    ),
                    'inputType' => 'text',
                    'eval' => array(
                        'tl_class' => 'w50 clr',
                        'mandatory' => true,
                    ),
                ),

                // TEXT
                'sliderItemIcon_title' => array(
                    'label' => array(
                        'de' => array('Bildtitel', 'Hier können Sie den Titel des Bildes eingeben (title-Attribut)'),
                    ),
                    'inputType' => 'text',
                    'eval' => array(
                        'tl_class' => 'w50',
                        'mandatory' => true,
                    ),
                ),
                'sliderItemTitle' => array(
                    'label' => array(
                        'de' => array('Überschrift', 'Hier können Sie die Überschrift für das Slider-Element einfügen'),
                    ),
                    'inputType' => 'text',
                    'eval' => array('tl_class' => 'w50 clr'),
                ),
                'sliderItemDescription' => array(
                    'label' => array(
                        'de' => array('Text', 'Hier können Sie den Text für das Slider-Element einfügen'),
                    ),
                    'inputType' => 'textarea',
                    'eval' => array(
                        'tl_class' => 'clr',
                        'rte' => 'tinyMCE'
                    ),

                ),
            ),
        ),
    ),
);