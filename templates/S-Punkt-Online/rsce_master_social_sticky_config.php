<?php
return array(
    'label' => array(
        'de' => array('Sidebar: Menü', 'Eine strukturierte Vorlage für Social Media Links'),
    ),

    'types' => array('content'),
    'contentCategory' => 'RSCE: NODE',
    //'beTemplate' => 'be_wildcard',
    'standardFields' => array('headline', 'cssID'),
    'wrapper' => array(
        'type' => 'none',
    ),
    

    'fields' => array(
        'button_image' => array(
            'label' => array(
                'de' => array('Button-Bild', 'Hier können Sie ein Bild für den Button auswählen'),
            ),
            'inputType' => 'fileTree',
            'eval' => array(
                'fieldType' => 'radio',
                'filesOnly' => true,
                'extensions' => 'jpg,jpeg,png,gif,svg',
                'tl_class' => 'w50',
            ),
        ),
        'contacts_list' => array(
            'label' => array(
                'de' => array('Liste der Kontakte ', 'Hier können Sie Kontakte ergänzen'),
            ),
            'elementLabel' => array(
                'de' => 'Kontakt %s',
            ),

            'inputType' => 'list',
            'fields' => array(
                'contact_icon' => array(
                    'label' => array(
                        'de' => array('Icon', 'Hier können Sie ein Icon für die Kontakten auswählen'),
                    ),
                    'inputType' => 'fileTree',
                    'eval' => array(
                        'fieldType' => 'radio',
                        'filesOnly' => true,
                        'extensions' => 'jpg,jpeg,png,gif,svg',
                        'tl_class' => 'w50',
                    ),
                ),
                'contact_icon_alt' => array(
                    'label' => array(
                        'de' => array('Alt-Attribut', 'Hier können Sie ein Alt-Attribut für das Icon einfügen'),
                    ),
                    'inputType' => 'text',
                    'eval' => array('tl_class' => 'w50'),
                ),
                'contact_link' => array(
                    'label' => array(
                        'de' => array('Link', 'Hier können Sie ein Link für den Kontakt einfügen'),
                    ),
                    'inputType' => 'url',
                    'eval' => array('tl_class' => 'w50 clr'),
                ),
                'contact_link_title' => array(
                    'label' => array(
                        'de' => array('Link-Title', 'Hier können Sie ein Link-Title für den Kontakt einfügen'),
                    ),
                    'inputType' => 'text',
                    'eval' => array('tl_class' => 'w50'),
                ),
                'contact_link_text' => array(
                    'label' => array(
                        'de' => array('Link-Text', 'Hier können Sie einen Link-Text für den Kontakt einfügen'),
                    ),
                    'inputType' => 'text',
                    'eval' => array('tl_class' => 'w50'),
                ),
            ),
        ),
        'description' => array(
            'label' => array('Beschreibungen', 'Fügen Sie hier Ihre Beschreibungstexte ein.'),
            'inputType' => 'group',
        ),

        'title' => array(
            'label' => array(
                'de' => array('Überschrift', 'Hier können Sie eine Überschrift einfügen'),
            ),
            'inputType' => 'text',
            'eval' => array('tl_class' => 'w50'),
        ),
        'text' => array(
            'label' => array(
                'de' => array('Text', 'Hier können Sie den Text einfügen'),
            ),
            'inputType' => 'textarea',
            'eval' => array(
                'tl_class' => 'clr',
                'rte' => 'tinyMCE'
            ),
        ),
        'link' => array(
            'label' => array(
                'de' => array('Button-Link', 'Hier können Sie einen Link auswählen'),
            ),
            'inputType' => 'url',
            'eval' => array('tl_class' => 'w50 clr'),
        ),
        'link_icon' => array(
            'label' => array(
                'de' => array('Icon', 'Hier können Sie das Bild auswählen'),
            ),
            'inputType' => 'fileTree',
            'eval' => array(
                'fieldType' => 'radio',
                'filesOnly' => true,
                'extensions' => 'jpg,jpeg,png,gif,svg',
                'tl_class'=>'w50',
            ),
        ),
        'link_text' => array(
            'label' => array(
                'de' => array('Link-Text', 'Hier können Sie einen Text für den Link eingeben'),
            ),
            'inputType' => 'text',
            'eval' => array('tl_class' => 'w50 clr'),
        ),
        'link_title' => array(
            'label' => array(
                'de' => array('Link-Title', 'Hier können Sie einen Titel für den Link eingeben'),
            ),
            'inputType' => 'text',
            'eval' => array('tl_class' => 'w50 clr'),
        ),
        'sm_group' => array(
            'label' => array('SM Beschreibungen', 'Fügen Sie hier Ihre Beschreibungstexte ein.'),
            'inputType' => 'group',
        ),
        'sm_title' => array(
            'label' => array(
                'de' => array('Überschrift', 'Hier können Sie eine Überschrift einfügen'),
            ),
            'inputType' => 'text',
            'eval' => array('tl_class' => 'w50'),
        ),
        'sm_list' => array(
            'label' => array(
                'de' => array('Liste der Social Media Links ', 'Hier können Sie Links ergänzen'),
            ),
            'elementLabel' => array(
                'de' => 'Social Media Link %s',
            ),

            'inputType' => 'list',
            'fields' => array(

                'icon' => array(
                    'label' => array(
                        'de' => array('Icon', 'Hier können Sie ein Icon für die Leistun auswählen'),
                    ),
                    'inputType' => 'fileTree',
                    'eval' => array(
                        'fieldType' => 'radio',
                        'filesOnly' => true,
                        'extensions' => 'jpg,jpeg,png,gif,svg',
                        'tl_class' => 'w50',
                    ),
                ),
                'icon_alt' => array(
                    'label' => array(
                        'de' => array('Alt-Attribut', 'Hier können Sie ein Alt-Attribut für das Icon einfügen'),
                    ),
                    'inputType' => 'text',
                    'eval' => array('tl_class' => 'w50'),
                ),
                'link' => array(
                    'label' => array(
                        'de' => array('Link', 'Hier können Sie ein Link für das Icon einfügen'),
                    ),
                    'inputType' => 'url',
                    'eval' => array('tl_class' => 'w50 clr'),
                ),
                'link_title' => array(
                    'label' => array(
                        'de' => array('Link-Title', 'Hier können Sie ein Link-Title für das Icon einfügen'),
                    ),
                    'inputType' => 'text',
                    'eval' => array('tl_class' => 'w50'),
                ),
            ),
        ),
    ),
);