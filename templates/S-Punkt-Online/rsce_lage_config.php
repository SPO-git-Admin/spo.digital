<?php

#####################################
### Created by S Punkt Online #######
### https://www.s-punkt-online.de ###
#####################################

return array(
    'label' => array(
        'Inhaltselement: Überschriftenliste mit Texten',
    ),
    'types' => array('content'),
    'contentCategory' => 'RSCE: Inhalt',
    //'beTemplate' => 'be_wildcard',
    'standardFields' => array('cssID'),
    'wrapper' => array(
        'type' => 'none',
    ),
    
    'fields' => array(
        /*** TEXT ***/
        'button' =>  array(
            'label' => array(
                'de' => array('Button-Text', 'Button, um die folgende Liste zu öffnen'),
            ),
            'inputType' => 'textarea',
            'eval' => array('tl_class' => 'w50 clr'),
        ),
        'button_icon' => array(
            'label' => array(
                'de' => array('Icon', 'Hier können Sie das Bild auswählen'),
            ),
            'inputType' => 'fileTree',
            'eval' => array(
                'fieldType' => 'radio',
                'filesOnly' => true,
                'extensions' => 'jpg,jpeg,png,gif,svg',
                'tl_class'=>'w50',
            ),
        ),
        /*** LIST ***/
        'list' => array(
            'label' => array('Überschriftenliste mit Texten', 'Fügen Sie Elemente hinzu.'),
            'elementLabel' => '%s. Item',
            'inputType' => 'list',
            'minItems' => 1,
            'fields' => array(
                /*** TEXT ***/
                'titel' =>  array(
                    'label' => array(
                        'de' => array('Überschrift', 'Hier können Sie eine Überschrift hinzufügen'),
                    ),
                    'inputType' => 'text',
                    'eval' => array('tl_class' => 'w50 clr'),
                ),

                /*** TEXTAREA ***/
                'text' =>  array(
                    'label' => array(
                        'de' => array('Beschreibung', 'Hier können Sie eine Beschreibung hinzufügen'),
                    ),
                    'inputType' => 'textarea',
                    'eval' => array('rte' => 'tinyMCE', 'tl_class' => 'clr'),
                ),
            ),
        ),

        /*** LIST - 2 ***/
        'list_1' => array(
            'label' => array('Weitere Überschriftenliste mit Texten', 'Fügen Sie Elemente hinzu.'),
            'elementLabel' => '%s. Item',
            'inputType' => 'list',
            'minItems' => 1,
            'fields' => array(
                /*** TEXT ***/
                'titel' =>  array(
                    'label' => array(
                        'de' => array('Überschrift', 'Hier können Sie eine Überschrift hinzufügen'),
                    ),
                    'inputType' => 'text',
                    'eval' => array('tl_class' => 'w50 clr'),
                ),

                /*** TEXTAREA ***/
                'text' =>  array(
                    'label' => array(
                        'de' => array('Beschreibung', 'Hier können Sie eine Beschreibung hinzufügen'),
                    ),
                    'inputType' => 'textarea',
                    'eval' => array('rte' => 'tinyMCE', 'tl_class' => 'clr'),
                ),
            ),
        ),
    ),
);