<?php
return array(
    'label' => array(
        'de' => array('Überschrift: Eine Vorlage für die Überschriften', ''),
    ),
    'types' => array('content'),
    'contentCategory' => 'RSCE: Inhalt',
    //'beTemplate' => 'be_wildcard',
    'standardFields' => array('headline', 'cssID'),
    'wrapper' => array(
        'type' => 'none',
    ),
   
    'fields' => array(
        'span' =>  array(
            'label' => array(
                'de' => array('Untertitel'),
            ),
            'inputType' => 'text',
            'eval' => array('tl_class'=>'w50 clr'),
        ),
        'gr_title' => array(
            'label' => array('Erscheinungsbild', 'die gewünschten Elemente auswählen, die auf der Seite angezeigt werden sollen'),
            'inputType' => 'group',
        ),
        'chb_title_container' => array(
            'label' => array(
                'de' => array('Überschrift im Container', 'Die Überschrift in einen Container einschließen'),
            ),
            'inputType' => 'checkbox',
        ),
        'chb_title_padding_top' => array(
            'label' => array(
                'de' => array('Oberer Einzug', 'Hinzufügen eines oberen Einzugs'),
            ),
            'inputType' => 'checkbox',
        ),

    ),
);