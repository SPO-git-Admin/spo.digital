<?php
return array(
    'label' => array(
        'de' => array('Akkordeon: eine Überschrift und eine Beschreibung ', ''),
    ),
    'types' => array('content'),
    'contentCategory' => 'RSCE: Inhalt',
    //'beTemplate' => 'be_wildcard',
    'standardFields' => array('headline', 'cssID'),
    'wrapper' => array(
        'type' => 'none',
    ),

    'fields' => array(

        'services' => array(
            'label' => array(
                'de' => array('Die Liste', 'Fügen Sie eine beliebige Anzahl an Items ein.'),
            ),

            'elementLabel' => array( 'de' => 'Item %s'),

            'inputType' => 'list',
            'fields' => array(

                'title' => array(
                    'label' => array(
                        'de' => array('Überschrift', 'Hier können Sie die Überschrift hinztufügen'),
                    ),
                    'inputType' => 'text',
                    'eval' => array(
                        'tl_class' => 'w50'
                    ),
                ),

                'description' => array(
                    'label' => array(
                        'de' => array('Beschreibung', 'Hier können Sie die Beschreibung hinzufügen'),
                    ),
                    'inputType' => 'textarea',
                    'eval' => array(
                        'rte' => 'tinyMCE',
                        'tl_class' => 'clr',
                    ),
                ),
            ),
        ),
    ),

);