<?php
return array(
    'label' => array(
        'de' => array('Titelmotiv: Ein Hintergrundbild in Teaser-Texten', ''),
    ),
    'types' => array('content'),
    'contentCategory' => 'RSCE: Inhalt',
    //'beTemplate' => 'be_wildcard',
    'standardFields' => array('cssID'),
    'wrapper' => array(
        'type' => 'none',
    ),
    

    'fields' => array(
        'image_desktop' => array(
            'label' => array(
                'de' => array('Bild für Desktop (max: 1920px)', 'Hier können Sie ein Bild auswählen'),
            ),
            'inputType' => 'fileTree',
            'eval' => array(
                'fieldType' => 'radio',
                'filesOnly' => true,
                'extensions' => 'jpg,jpeg,png,gif,svg',
            ),
        ),
        /* 'size_desktop' => array(
            'label' => array('Bildbreite und Bildhöhe', ''),
            'inputType' => 'imageSize',
            'options' => \System::getImageSizes(),
            'reference' => &$GLOBALS['TL_LANG']['MSC'],
            'eval' => array(
                'rgxp' => 'digit',
                'includeBlankOption' => true,
                'tl_class' => 'w50',
            ),
        ), */
        'image_header_alt' => array(
            'label' => array(
                'de' => array('Alt-Attribute', 'Hier können Sie einen Alt-Attribute für das Bild eingeben'),
            ),
            'inputType' => 'text',
            'eval' => array(
                'tl_class' => 'w50 clr',
                'mandatory' => true,
            ),
        ),
        'image_header_title' => array(
            'label' => array(
                'de' => array('Bildtitel', 'Hier können Sie den Titel des Bildes eingeben (title-Attribut)'),
            ),
            'inputType' => 'text',
            'eval' => array(
                'tl_class' => 'w50 clr',
                'mandatory' => true,
            ),
        ),
        'description_group' => array(
            'label' => array('Beschreibungen', 'Fügen Sie hier Ihre Beschreibungstexte ein.'),
            'inputType' => 'group',
        ),
        'title' => array(
            'label' => array(
                'de' => array('Überschrift', 'Hier können Sie eine Überschrift einfügen'),
            ),
            'inputType' => 'text',
            'eval' => array('tl_class' => 'w50 clr'),
        ),
        'description' => array(
            'label' => array('Beschreibung', 'Ausführliche Beschreibung der Stelle'),
            'eval' => array('rte' => 'tinyMCE', 'tl_class' => "clr"),
            'inputType' => 'textarea',
        ),
        'link' => array(
            'label' => array(
                'de' => array('Link', 'Hier können Sie ein Link einfügen'),
            ),
            'inputType' => 'url',
            'eval' => array('tl_class' => 'w50 clr'),
        ),
        'link_icon' => array(
            'label' => array(
                'de' => array('Icon', 'Hier können Sie das Bild auswählen'),
            ),
            'inputType' => 'fileTree',
            'eval' => array(
                'fieldType' => 'radio',
                'filesOnly' => true,
                'extensions' => 'jpg,jpeg,png,gif,svg',
                'tl_class'=>'w50',
            ),
        ),
        'link_title' => array(
            'label' => array(
                'de' => array('Link-Title', 'Hier können Sie ein Link-Title einfügen'),
            ),
            'inputType' => 'text',
            'eval' => array('tl_class' => 'w50 clr'),
        ),
        'link_text' => array(
            'label' => array(
                'de' => array('Link-Text', 'Hier können Sie einen Link-Text einfügen'),
            ),
            'inputType' => 'text',
            'eval' => array('tl_class' => 'w50 clr'),
        ),
    ),
);