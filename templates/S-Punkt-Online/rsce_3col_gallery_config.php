<?php
#####################################
### Created by S Punkt Online #######
### https://www.s-punkt-online.de ###
#####################################

return array (
    'label' => array( 'Galerie: dreispaltiger Container' ),
    'types' => array( 'content' ),
    'contentCategory' => 'RSCE: Referenzen',
    'standardFields' => array( 'cssID' ),
    'wrapper' => array(
        'type' => 'none',
    ),

    'fields' => array(
        ### group ###
        'hex_group' => array(
            'label' => array( 'Gradient-Farbe', 'Hier können Sie Gradient hinzufügen.' ),
            'inputType' => 'group',
        ),

        ### text ###
        'hex' => array(
            'label' => array(
                'de' => array('Gradient-Farbe von:', 'Hier können Sie HEX eingeben'),
            ),
            'inputType' => 'text',
            'eval' => array(
                'tl_class' => 'w50 clr',
                'mandatory' => true
            ),
        ),

        ### text ###
        'hex_1' => array(
            'label' => array(
                'de' => array('Gradient-Farbe bis:', 'Hier können Sie HEX eingeben'),
            ),
            'inputType' => 'text',
            'eval' => array(
                'tl_class' => 'w50',
                'mandatory' => true
            ),
        ),

        'list' => array(
            'label' => array('Bilder-Liste', 'Hier können Sie Bilder hinzufügen'),
            'elementLabel' => '%s. Bild Item',
            'inputType' => 'list',
            'minItems' => 1,
            'maxItems' => 3,
            'fields' => array(

                ### image ###
                'image' => array(
                    'label' => array(
                        'de' => array('Bild', 'Hier können Sie das Bild auswählen'),
                    ),
                    'inputType' => 'fileTree',
                    'eval' => array(
                        'fieldType' => 'radio',
                        'filesOnly' => true,
                        'extensions' => 'jpg,jpeg,png,gif,svg',
                        'tl_class'=>'clr',
                        'mandatory' => true,
                    ),
                ),

                ### image alt ###
                'image_alt' => array(
                    'label' => array(
                        'de' => array('Alt-Attribute', 'Hier können Sie einen Alt-Attribute für das Bild eingeben'),
                    ),
                    'inputType' => 'text',
                    'eval' => array(
                        'tl_class' => 'w50 clr',
                        'mandatory' => true,
                    ),
                ),

                ### image title ###
                'image_title' => array(
                    'label' => array(
                        'de' => array('Bildtitel', 'Hier können Sie den Titel des Bildes eingeben (title-Attribut)'),
                    ),
                    'inputType' => 'text',
                    'eval' => array(
                        'tl_class' => 'w50 clr',
                        'mandatory' => true,
                    ),
                ),
            ),
        ),
    ),
);