<?php
#####################################
### Created by S Punkt Online #######
### https://www.s-punkt-online.de ###
#####################################

return array(
    'label' => array(
        'de' => array('Button: Link, Titel, Icon, Positionierung', ''),
    ),
    'types' => array('content'),
    'contentCategory' => 'RSCE: Inhalt',
    'beTemplate' => 'be_wildcard',
    'standardFields' => array('cssID'),
    'wrapper' => array(
        'type' => 'none',
    ),

    'fields' => array(

        // GROUP;
        'button_group_position' => array(
            'label' => array('Button-Positionierung', 'Button Links als Standardeinstellung'),
            'inputType' => 'group',
        ),

        // RADIO;
        'button_radio' => array(
            'label' => array('Erscheinungsbild für den Button'),
            'inputType' => 'radio',
            'default' => 'button_left',
            'options' => array(
                'button_left' => 'Links',
                'button_center' => 'Mitte',
                'button_right' => 'Rechts',
            ),
            'eval' => array(
                'tl_class' => 'w50',
            ),
        ),

        // GROUP;
        'button_group' => array(
            'label' => array('Button', 'Hier können Sie Link, Titel, Text für den Button'),
            'inputType' => 'group',
        ),


        // LINK;
        'button' => array(
            'label' => array(
                'de' => array('Button', 'Hier können Sie das Link auswählen'),
                'en' => array('Link'),
            ),
            'inputType' => 'url',
            'eval' => array(
                'tl_class' => 'w50 clr',
                'mandatory' => true
            ),
        ),

        // IMAGE;
        'button_icon' => array(
            'label' => array(
                'de' => array('Button-Icon', 'Hier können Sie das Bild auswählen'),
            ),
            'inputType' => 'fileTree',
            'eval' => array(
                'fieldType' => 'radio',
                'filesOnly' => true,
                'extensions' => 'jpg,jpeg,png,gif,svg',
                'tl_class'=>'w50',
                'mandatory' => true
            ),
        ),

        // TEXT;
        'button_text' => array(
            'label' => array(
                'de' => array('Button Text', 'Hier können Sie einen Text hinzufügen'),
                'en' => array('Link-Text'),
            ),
            'inputType' => 'text',
            'eval' => array(
                'tl_class' => 'w50',
                'mandatory' => true
                ),
        ),

        // TEXT;
        'button_title' => array(
            'label' => array(
                'de' => array('Button-Title', 'Hier können Sie einen Text hinzufügen'),
                'en' => array('Link-Text'),
            ),
            'inputType' => 'text',
            'eval' => array(
                'tl_class' => 'w50',
                'mandatory' => true
            ),
        ),
    ),
);