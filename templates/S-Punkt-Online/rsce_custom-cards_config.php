<?php
return array(
    'label' => array('Preview-Kästchen: Bild, Überschrift, Link-Button', ''),
    'types' => array('content'),
    'contentCategory' => 'RSCE: Inhalt',
    //'beTemplate' => 'be_wildcard',
    'standardFields' => array('headline', 'cssID'),
    'wrapper' => array(
        'type' => 'none',
    ),

    'fields' => array(
        'cardsList' => array(
            'label' => array(
                'de' => array(
                    'Cards',
                    'Fügen Sie eine beliebige Anzahl an Bilder mit einem Text und Link ein.',
                ),
            ),
            'elementLabel' => array(
                'de' => 'Card-Item %s',
            ),
            'inputType' => 'list',
            'fields' => array(
                'itemImage' => array(
                    'label' => array(
                        'de' => array('Hintergrundbild', 'Hier können Sie das Bild auswählen'),
                    ),
                    'inputType' => 'fileTree',
                    'eval' => array(
                        'fieldType' => 'radio',
                        'filesOnly' => true,
                        'extensions' => 'jpg,jpeg,png,gif,svg',
                        'tl_class'=>'w50 clr',
                    ),
                ),
                'itemTitle' => array(
                    'label' => array(
                        'de' => array('Überschrift', 'Hier können Sie einen Text eingeben'),
                    ),
                    'inputType' => 'text',
                    'eval' => array(
                        'tl_class' => 'w50 clr',
                    ),
                ),
                'button' => array(
                    'label' => array('Button', 'Füllen Sie die erforderlichen Felder aus'),
                    'inputType' => 'group',
                ),

                // LINK //
                'link' => array(
                    'label' => array(
                        'de' => array('Button-Link', 'Hier können Sie einen Link auswählen'),
                    ),
                    'inputType' => 'url',
                    'eval' => array('tl_class' => 'w50 clr'),
                ),

                // IMAGE //
                'link_icon' => array(
                    'label' => array(
                        'de' => array('Icon', 'Hier können Sie das Bild auswählen'),
                    ),
                    'inputType' => 'fileTree',
                    'eval' => array(
                        'fieldType' => 'radio',
                        'filesOnly' => true,
                        'extensions' => 'jpg,jpeg,png,gif,svg',
                        'tl_class'=>'w50',
                    ),
                ),

                // TEXT //
                'link_text' => array(
                    'label' => array(
                        'de' => array('Link-Text', 'Hier können Sie einen Text für den Link eingeben'),
                    ),
                    'inputType' => 'text',
                    'eval' => array('tl_class' => 'w50 clr'),
                ),

                // TEXT //
                'link_title' => array(
                    'label' => array(
                        'de' => array('Link-Title', 'Hier können Sie einen Titel für den Link eingeben'),
                    ),
                    'inputType' => 'text',
                    'eval' => array('tl_class' => 'w50 clr'),
                ),
            ),
        ),
    ),
);