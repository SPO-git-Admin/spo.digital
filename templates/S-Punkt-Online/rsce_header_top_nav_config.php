<?php
return array(
    'label' => array(
        'de' => array('Header: Liste der Top-Navigation', ''),
    ),

    'types' => array('content'),
    'contentCategory' => 'RSCE: NODE',
    //'beTemplate' => 'be_wildcard',
    'standardFields' => array('cssID'),
    'wrapper' => array(
        'type' => 'none',
    ),
    

    'fields' => array(
        'contacts' => array(
            'label' => array(
                'de' => array('Liste der Kontakte', 'Hier können Sie Kontakte ergänzen'),
            ),
            'elementLabel' => array(
                'de' => 'Kontakt %s',
            ),

            'inputType' => 'list',
            'fields' => array(
                'icon' => array(
                    'label' => array(
                        'de' => array('Icon', 'Hier können Sie ein Icon auswählen'),
                    ),
                    'inputType' => 'fileTree',
                    'eval' => array(
                        'fieldType' => 'radio',
                        'filesOnly' => true,
                        'extensions' => 'jpg,jpeg,png,gif,svg',
                        'mandatory' => true,
                        'tl_class' => 'w50',
                    ),
                ),
                'icon_alt' => array(
                    'label' => array(
                        'de' => array('Alt-Attribute', 'Hier können Sie Alt-Attribute für das Logo hinzufügen'),
                    ),
                    'inputType' => 'text',
                    'eval' => array('tl_class' => 'w50', 'mandatory' => true),
                ),
                'link' => array(
                    'label' => array('Link', 'Hier können Sie das Link auswählen'),
                    'inputType' => 'url',
                    'eval' => array('tl_class' => 'w50 clr', 'mandatory' => true),
                ),
                'link_title' => array(
                    'label' => array(
                        'de' => array('Link-Title', 'Hier können Sie Link-Title hinzufügen'),
                    ),
                    'inputType' => 'text',
                    'eval' => array('tl_class' => 'w50', 'mandatory' => true),
                ),
                'link_text' => array(
                    'label' => array(
                        'de' => array('Link-Text', 'Hier können Sie Link-Text hinzufügen'),
                    ),
                    'inputType' => 'text',
                    'eval' => array('tl_class' => 'w50', 'mandatory' => true),
                ),
            ),
        ),
    ),
);