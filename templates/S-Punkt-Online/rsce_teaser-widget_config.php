<?php
return array(
    'label' => array(
        'de' => array(
            'Teaser-Widget: Text links, Bild rechts',
            'Bild mit einem Text und Link',
        ),
        'en' => array(
            'Custom: Teaser-Widget (Text left, Image right)',
            'Image with Text and Link',
        ),
    ),

    'types' => array('content'),
    'contentCategory' => 'RSCE: Inhalt',
    //'beTemplate' => 'be_wildcard',
    'standardFields' => array('cssID'),
    'wrapper' => array(
        'type' => 'none',
    ),

    'fields' => array(
        'image' => array(
            'label' => array(
                'de' => array('Hintergrundbild', 'Hier können Sie das Bild auswählen'),
                'en' => array('Background-Image'),
            ),
            'inputType' => 'fileTree',
            'eval' => array(
                'fieldType' => 'radio',
                'filesOnly' => true,
                'extensions' => 'jpg,jpeg,png,gif,svg',
            ),
        ),

        'title' => array(
            'label' => array(
                'de' => array('Überschrift', 'Hier können Sie einen Text einfügenein'),
            ),

            'inputType' => 'text',
            'eval' => array('tl_class' => 'w50'),
        ),

        'description' =>  array(
            'label' => array(
                'de' => array('Beschreibung', 'Hier können Sie einen Text einfügenein'),
            ),
            'inputType' => 'text',
            'eval' => array('tl_class' => 'w50'),
        ),

        'link' => array(
            'label' => array(
                'de' => array('Button-Link', 'Hier können Sie ein Link auswählen'),
                'en' => array('Button-Link'),
            ),
            'inputType' => 'url',
            'eval' => array('tl_class' => 'w50'),
        ),

        'linkText' => array(
            'label' => array(
                'de' => array('Button-Text', 'Hier können Sie den Text für Button schreiben'),
                'en' => array('Button-Text'),
            ),
            'inputType' => 'text',
            'eval' => array('tl_class' => 'w50'),
        ),
        'css' => array(
            'label' => array(
                'de' => array('CSS-Klasse'),
            ),
            'inputType' => 'text',
            'eval' => array('tl_class'=>'w50 clr'),
        ),
    ),
);
