<?php

#####################################
### Created by S Punkt Online #######
### https://www.s-punkt-online.de ###
#####################################

return array(
    'label' => array(
        'Hauptinhalt Container: Bild, Title, Text, 2-Col, Galerie, Button',
    ),
    'types' => array('content'),
    'contentCategory' => 'RSCE: Inhalt',
    //'beTemplate' => 'be_wildcard',
    'standardFields' => array('headline', 'cssID'),
    'wrapper' => array(
        'type' => 'none',
    ),
    'fields' => array(
        'subtitle' => array(
            'label' => array(
                'de' => array('Untertitel', 'Hier können Sie einen Text hinzufügen'),
            ),
            'inputType' => 'text',
            'eval' => array('tl_class' => 'clr w50'),
        ),

        /******************
         * CHECKBOX BLOCK *
         ******************/
        'checkbox_list' => array(
            'label' => array('Erscheinungsbild', 'die gewünschten Elemente auswählen, die auf der Seite angezeigt werden sollen'),
            'inputType' => 'group',
        ),

        'chb_image_header' => array(
            'label' => array(
                'de' => array('Bild in der Kopfzeile', 'Ein Bild zur Kopfzeile hinzufügen'),
            ),
            'inputType' => 'checkbox',
        ),

        'checkbox_ul' => array(
            'label' => array(
                'de' => array('Stilisiertes Kästchen', 'Elemente ohne Stilisierung als Standardeinstellung'),
            ),
            'inputType' => 'checkbox',
        ),

        'checkbox_background' => array(
            'label' => array(
                'de' => array('Hintergrundfarbe', 'Container ohne Hintergrundfarbe als Standardeinstellung'),
            ),
            'inputType' => 'checkbox',
        ),

        'checkbox_background_row' => array(
            'label' => array(
                'de' => array('Hintergrundfarbe für beide Spalten', 'Spalten ohne Hintergrundfarbe'),
            ),
            'inputType' => 'checkbox',
        ),

        'checkbox_col' => array(
            'label' => array(
                'de' => array('Zweispaltiger Container', 'Container ohne Spaltenelemente als Standardeinstellung'),
            ),
            'inputType' => 'checkbox',
        ),
        'checkbox_3_col' => array(
            'label' => array(
                'de' => array('Dritte Spalte hinzufügen (Wird nur angezeigt, wenn die anderen Spalten aktiviert sind)', 'Fügt eine dritte Spalte am Ende des Containers ein'),
            ),
            'inputType' => 'checkbox',
        ),

        'text_top_checkbox' => array(
            'label' => array(
                'de' => array('Extra-Text oben', 'Standardmäßig kein Text im oberen Bereich'),
            ),
            'inputType' => 'checkbox',
        ),
        // CHECKBOX //
        'text_bottom_checkbox' => array(
            'label' => array(
                'de' => array('Extra-Text unten', 'Standardmäßig kein Text im oberen Bereich'),
            ),
            'inputType' => 'checkbox',
        ),

        /****************
         * HEADER IMAGE *
         ****************/

        'gr_image_header' => array(
            'label' => array('Bild in der Kopfzeile', 'Ein Bild zur Kopfzeile hinzufügen'),
            'inputType' => 'group',
            'dependsOn' => [
                'field' => 'chb_image_header',
            ],
        ),
        'image_header' => array(
            'label' => array(
                'de' => array('Quelldatei: (1920px * 500px)', 'Hier können Sie das Bild auswählen'),
            ),
            'inputType' => 'fileTree',
            'eval' => array(
                'fieldType' => 'radio',
                'filesOnly' => true,
                'extensions' => 'jpg,jpeg,png,gif,svg',
                'tl_class'=>'w50',
                'mandatory' => true,
            ),
        ),
        'image_header_alt' => array(
            'label' => array(
                'de' => array('Alt-Attribute', 'Hier können Sie einen Alt-Attribute für das Bild eingeben'),
            ),
            'inputType' => 'text',
            'eval' => array(
                'tl_class' => 'w50 clr',
                'mandatory' => true
            ),
        ),
        'image_header_title' => array(
            'label' => array(
                'de' => array('Bildtitel', 'Hier können Sie den Titel des Bildes eingeben (title-Attribut)'),
            ),
            'inputType' => 'text',
            'eval' => array(
                'tl_class' => 'w50 clr',
                'mandatory' => true
            ),
        ),

        /******************
         * EXTRA TEXT TOP *
         ******************/

        // GROUP //
        'text' => array(
            'label' => array('Extra Text Oben', 'Fügen Sie hier einen Text ein.'),
            'inputType' => 'group',
            'dependsOn' => [
                'field' => 'text_top_checkbox',
            ],
        ),

        'text_top_ul_deco' => array(
            'label' => array(
                'de' => array('Stilisiertes Kästchen', 'Elemente ohne Stilisierung als Standardeinstellung'),
            ),
            'inputType' => 'checkbox',
        ),

        // TEXTAREA //
        'text_top' => array(
            'label' => array('Beschreibung: Der Text nimmt die gesamte Breite des Containers ein', 'Hier können Sie die Beschreibung hinzufügen'),
            'eval' => array('rte' => 'tinyMCE'),
            'inputType' => 'textarea',
            'tl_class' => 'clr',
            'dependsOn' => [
                'field' => 'text_top_checkbox',
            ],
        ),

        /****************
         * FIRST COL *
         ****************/

        'col_1' => array(
            'label' => array('Erste Spalte', 'Fügen Sie hier einen Text und Bild ein.'),
            'inputType' => 'group',
            'dependsOn' => [
                'field' => 'checkbox_col',
            ],
        ),

        'col_1_radio' => array(
            'label' => array('Elementtyp auswählen', 'Hier können Sie die Elementtypen auswählen, die auf der Seite angezeigt werden sollen. '),
            'inputType' => 'radio',
            'default' => 'text',
            'options' => array(
                'text' => 'Text',
                'image' => 'Bild',
            ),
        ),

        'col_1_bcg_check' => array(
            'label' => array(
                'de' => array('Hintergrund für den Text-Block', 'Standardmäßig Text-Block ohne Hintergrund'),
            ),
            'inputType' => 'checkbox',
        ),

        'col_1_bubble_check' => array(
            'label' => array(
                'de' => array('Text-Block als die Blase', 'Standardmäßig Text-Block'),
            ),
            'inputType' => 'checkbox',
        ),
        'col_1_chb_check' => array(
            'label' => array(
                'de' => array('Stilisiertes Kästchen', 'Elemente ohne Stilisierung als Standardeinstellung'),
            ),
            'inputType' => 'checkbox',
        ),

        // GROUP //
        'col_1_checkbox_button' => array(
            'label' => array(
                'de' => array('Button hinzufügen', 'Container ohne Button als Standardeinstellung'),
            ),
            'inputType' => 'checkbox',
        ),

        'col_1_text' => array(
            'label' => array('Beschreibung', 'Hier können Sie die Beschreibung hinzufügen'),
            'eval' => array('rte' => 'tinyMCE'),
            'inputType' => 'textarea',
            'tl_class' => 'clr',
            'dependsOn' => [
                'field' => 'col_1_radio',
                'value' => 'text',
            ],
        ),

        /**********
         * BUTTON *
         **********/


        // LINK //
        'col_1_link' => array(
            'label' => array(
                'de' => array('Button-Link', 'Hier können Sie einen Link auswählen'),
            ),
            'inputType' => 'url',
            'eval' => array('tl_class' => 'w50 clr'),
            'dependsOn' => [
                'field' => 'col_1_checkbox_button',
            ],
        ),

        // IMAGE //
        'col_1_link_icon' => array(
            'label' => array(
                'de' => array('Icon', 'Hier können Sie das Bild auswählen'),
            ),
            'inputType' => 'fileTree',
            'eval' => array(
                'fieldType' => 'radio',
                'filesOnly' => true,
                'extensions' => 'jpg,jpeg,png,gif,svg',
                'tl_class'=>'w50',
            ),
            'dependsOn' => [
                'field' => 'col_1_checkbox_button',
            ],
        ),

        // TEXT //
        'col_1_link_text' => array(
            'label' => array(
                'de' => array('Link-Text', 'Hier können Sie einen Text für den Link eingeben'),
            ),
            'inputType' => 'text',
            'eval' => array('tl_class' => 'w50 clr'),
            'dependsOn' => [
                'field' => 'col_1_checkbox_button',
            ],
        ),

        // TEXT //
        'col_1_link_title' => array(
            'label' => array(
                'de' => array('Link-Title', 'Hier können Sie einen Titel für den Link eingeben'),
            ),
            'inputType' => 'text',
            'eval' => array('tl_class' => 'w50 clr'),
            'dependsOn' => [
                'field' => 'col_1_checkbox_button',
            ],
        ),

        'col_1_image' => array(
            'label' => array(
                'de' => array('Bild', 'Hier können Sie das Bild auswählen.'),
            ),
            'inputType' => 'fileTree',
            'eval' => array(
                'fieldType' => 'radio',
                'filesOnly' => true,
                'extensions' => 'jpg,jpeg,png,gif,svg',
                'tl_class'=>'w50',
            ),
            'dependsOn' => [
                'field' => 'col_1_radio',
                'value' => 'image',
            ],
        ),
        'col_1_image_alt' => array(
            'label' => array(
                'de' => array('Alt-Attribute', 'Hier können Sie einen Alt-Attribute für das Bild eingeben'),
            ),
            'inputType' => 'text',
            'eval' => array(
                'tl_class' => 'w50 clr',
                'mandatory' => true,
            ),
            'dependsOn' => [
                'field' => 'col_1_radio',
                'value' => 'image',
            ],
        ),
        'col_1_image_title' => array(
            'label' => array(
                'de' => array('Bildtitel', 'Hier können Sie den Titel des Bildes eingeben (title-Attribut)'),
            ),
            'inputType' => 'text',
            'eval' => array(
                'tl_class' => 'w50',
                'mandatory' => true,
            ),
            'dependsOn' => [
                'field' => 'col_1_radio',
                'value' => 'image',
            ],
        ),

        /****************
         * SECOND COL *
         ****************/

        // GROUP //
        'col_2' => array(
            'label' => array('Zweite Spalte', 'Fügen Sie hier einen Text ein.'),
            'inputType' => 'group',
            'dependsOn' => [
                'field' => 'checkbox_col',
            ],
        ),

        // RADIO //
        'col_2_radio' => array(
            'label' => array('Elementtyp auswählen', 'Hier können Sie die Elementtypen auswählen, die auf der Seite angezeigt werden sollen. '),
            'inputType' => 'radio',
            'default' => 'text',
            'options' => array(
                'text' => 'Text',
                'image' => 'Bild',
            ),
        ),
        // CHECKBOX //
        'col_2_checkbox' => array(
            'label' => array(
                'de' => array('Spalte nach oben (Mobile)', 'Die Spalte befindet sich standardmäßig am unteren Rand'),
            ),
            'inputType' => 'checkbox',
            'dependsOn' => [
                'field' => 'checkbox_col',
            ],
        ),
        // CHECKBOX //
        'col_2_bcg_check' => array(
            'label' => array(
                'de' => array('Hintergrund für den Text-Block', 'Standardmäßig Text-Block ohne Hintergrund'),
            ),
            'inputType' => 'checkbox',
        ),

        // CHECKBOX //
        'col_2_bubble_check' => array(
            'label' => array(
                'de' => array('Text-Block als die Blase', 'Standardmäßig Text-Block'),
            ),
            'inputType' => 'checkbox',
        ),

        'col_2_chb_check' => array(
            'label' => array(
                'de' => array('Stilisiertes Kästchen', 'Elemente ohne Stilisierung als Standardeinstellung'),
            ),
            'inputType' => 'checkbox',
        ),

        // GROUP //
        'col_2_checkbox_button' => array(
            'label' => array(
                'de' => array('Button hinzufügen', 'Container ohne Button als Standardeinstellung'),
            ),
            'inputType' => 'checkbox',
        ),


        // TEXTAREA //
        'col_2_text' => array(
            'label' => array('Beschreibung', 'Hier können Sie die Beschreibung hinzufügen'),
            'eval' => array('rte' => 'tinyMCE'),
            'inputType' => 'textarea',
            'tl_class' => 'clr',
            'dependsOn' => [
                'field' => 'col_2_radio',
                'value' => 'text'
            ],
        ),

        /**********
         * BUTTON *
         **********/


        // LINK //
        'col_2_link' => array(
            'label' => array(
                'de' => array('Button-Link', 'Hier können Sie einen Link auswählen'),
            ),
            'inputType' => 'url',
            'eval' => array('tl_class' => 'w50 clr'),
            'dependsOn' => [
                'field' => 'col_2_checkbox_button',
            ],
        ),

        // IMAGE //
        'col_2_link_icon' => array(
            'label' => array(
                'de' => array('Icon', 'Hier können Sie das Bild auswählen'),
            ),
            'inputType' => 'fileTree',
            'eval' => array(
                'fieldType' => 'radio',
                'filesOnly' => true,
                'extensions' => 'jpg,jpeg,png,gif,svg',
                'tl_class'=>'w50',
            ),
            'dependsOn' => [
                'field' => 'col_2_checkbox_button',
            ],
        ),

        // TEXT //
        'col_2_link_text' => array(
            'label' => array(
                'de' => array('Link-Text', 'Hier können Sie einen Text für den Link eingeben'),
            ),
            'inputType' => 'text',
            'eval' => array('tl_class' => 'w50 clr'),
            'dependsOn' => [
                'field' => 'col_2_checkbox_button',
            ],
        ),

        // TEXT //
        'col_2_link_title' => array(
            'label' => array(
                'de' => array('Link-Title', 'Hier können Sie einen Titel für den Link eingeben'),
            ),
            'inputType' => 'text',
            'eval' => array('tl_class' => 'w50 clr'),
            'dependsOn' => [
                'field' => 'col_2_checkbox_button',
            ],
        ),

        // IMAGE //
        'col_2_image' => array(
            'label' => array(
                'de' => array('Bild', 'Hier können Sie das Bild auswählen.'),
            ),
            'inputType' => 'fileTree',
            'eval' => array(
                'fieldType' => 'radio',
                'filesOnly' => true,
                'extensions' => 'jpg,jpeg,png,gif,svg',
                'tl_class'=>'w50',
            ),
            'dependsOn' => [
                'field' => 'col_2_radio',
                'value' => 'image',
            ],
        ),
        'col_2_image_alt' => array(
            'label' => array(
                'de' => array('Alt-Attribute', 'Hier können Sie einen Alt-Attribute für das Bild eingeben'),
            ),
            'inputType' => 'text',
            'eval' => array(
                'tl_class' => 'w50 clr',
                'mandatory' => true,
            ),
            'dependsOn' => [
                'field' => 'col_2_radio',
                'value' => 'image',
            ],
        ),
        'col_2_image_title' => array(
            'label' => array(
                'de' => array('Bildtitel', 'Hier können Sie den Titel des Bildes eingeben (title-Attribut)'),
            ),
            'inputType' => 'text',
            'eval' => array('tl_class' => 'w50'),
            'dependsOn' => [
                'field' => 'col_2_radio',
                'value' => 'image',
            ],
        ),


        /****************
         * THIRD COL *
         ****************/

        // GROUP //
        'col_3' => array(
            'label' => array('Dritte Spalte', 'Fügen Sie hier einen Text und Bild ein.'),
            'inputType' => 'group',
            'dependsOn' => [
                'field' => 'checkbox_3_col',
            ],
        ),

        // RADIO //
        'col_3_radio' => array(
            'label' => array('Elementtyp auswählen', 'Hier können Sie die Elementtypen auswählen, die auf der Seite angezeigt werden sollen. '),
            'inputType' => 'radio',
            'default' => 'text',
            'options' => array(
                'text' => 'Text',
                'image' => 'Bild',
            ),
        ),

        // CHECKBOX //
        'col_3_bcg_check' => array(
            'label' => array(
                'de' => array('Hintergrund für den Text-Block', 'Standardmäßig Text-Block ohne Hintergrund'),
            ),
            'inputType' => 'checkbox',
        ),

        // CHECKBOX //
        'col_3_bubble_check' => array(
            'label' => array(
                'de' => array('Text-Block als die Blase', 'Standardmäßig Text-Block'),
            ),
            'inputType' => 'checkbox',
        ),

        // TEXTAREA //
        'col_3_text' => array(
            'label' => array('Beschreibung', 'Hier können Sie die Beschreibung hinzufügen'),
            'eval' => array('rte' => 'tinyMCE'),
            'inputType' => 'textarea',
            'tl_class' => 'clr',
            'dependsOn' => [
                'field' => 'col_2_radio',
                'value' => 'text',
            ],
        ),

        // IMAGE //
        'col_3_image' => array(
            'label' => array(
                'de' => array('Bild', 'Hier können Sie das Bild auswählen.'),
            ),
            'inputType' => 'fileTree',
            'eval' => array(
                'fieldType' => 'radio',
                'filesOnly' => true,
                'extensions' => 'jpg,jpeg,png,gif,svg',
                'tl_class'=>'w50',
            ),
            'dependsOn' => [
                'field' => 'col_3_radio',
                'value' => 'image',
            ],
        ),
        'col_3_image_alt' => array(
            'label' => array(
                'de' => array('Alt-Attribute', 'Hier können Sie einen Alt-Attribute für das Bild eingeben'),
            ),
            'inputType' => 'text',
            'eval' => array(
                'tl_class' => 'w50 clr',
                'mandatory' => true,
            ),
            'dependsOn' => [
                'field' => 'col_3_radio',
                'value' => 'image',
            ],
        ),
        'col_3_image_title' => array(
            'label' => array(
                'de' => array('Bildtitel', 'Hier können Sie den Titel des Bildes eingeben (title-Attribut)'),
            ),
            'inputType' => 'text',
            'eval' => array(
                'tl_class' => 'w50',
                'mandatory' => true,
            ),
            'dependsOn' => [
                'field' => 'col_3_radio',
                'value' => 'image',
            ],
        ),

        /*********************
         * EXTRA TEXT BOTTOM *
         *********************/

        // GROUP //
        'gr_text_bottom' => array(
            'label' => array('Extra Text Unten', 'Fügen Sie hier einen Text ein.'),
            'inputType' => 'group',
            'dependsOn' => [
                'field' => 'text_bottom_checkbox',
            ],
        ),

        // CHECKBOX;
        'text_bottom_ul_deco' => array(
            'label' => array(
                'de' => array('Stilisiertes Kästchen', 'Elemente ohne Stilisierung als Standardeinstellung'),
            ),
            'inputType' => 'checkbox',
        ),

        // TEXTAREA //
        'text_bottom' => array(
            'label' => array('Beschreibung: Der Text nimmt die gesamte Breite des Containers ein', 'Hier können Sie die Beschreibung hinzufügen'),
            'eval' => array('rte' => 'tinyMCE'),
            'inputType' => 'textarea',
            'tl_class' => 'clr',
            'dependsOn' => [
                'field' => 'text_bottom_checkbox',
            ],
        ),

        /**********
         * BUTTON *
         **********/

        // GROUP //
        'button' => array(
            'label' => array('Button', 'Füllen Sie die erforderlichen Felder aus'),
            'inputType' => 'group',
        ),
        'checkbox_button' => array(
            'label' => array(
                'de' => array('Button hinzufügen', 'Container ohne Button als Standardeinstellung'),
            ),
            'inputType' => 'checkbox',
        ),

        // LINK //
        'link' => array(
            'label' => array(
                'de' => array('Button-Link', 'Hier können Sie einen Link auswählen'),
            ),
            'inputType' => 'url',
            'eval' => array('tl_class' => 'w50 clr'),
            'dependsOn' => [
                'field' => 'checkbox_button',
            ],
        ),

        // IMAGE //
        'link_icon' => array(
            'label' => array(
                'de' => array('Icon', 'Hier können Sie das Bild auswählen'),
            ),
            'inputType' => 'fileTree',
            'eval' => array(
                'fieldType' => 'radio',
                'filesOnly' => true,
                'extensions' => 'jpg,jpeg,png,gif,svg',
                'tl_class'=>'w50',
            ),
            'dependsOn' => [
                'field' => 'checkbox_button',
            ],
        ),

        // TEXT //
        'link_text' => array(
            'label' => array(
                'de' => array('Link-Text', 'Hier können Sie einen Text für den Link eingeben'),
            ),
            'inputType' => 'text',
            'eval' => array('tl_class' => 'w50 clr'),
            'dependsOn' => [
                'field' => 'checkbox_button',
            ],
        ),

        // TEXT //
        'link_title' => array(
            'label' => array(
                'de' => array('Link-Title', 'Hier können Sie einen Titel für den Link eingeben'),
            ),
            'inputType' => 'text',
            'eval' => array('tl_class' => 'w50 clr'),
            'dependsOn' => [
                'field' => 'checkbox_button',
            ],
        ),


        /***********
         * GALLERY *
         ***********/

        // GROUP //
        'gallery_group' => array(
            'label' => array('Galerie', 'Fügen Sie hier Bilder ein.'),
            'inputType' => 'group',
        ),

        // CHECKBOX //
        'checkbox_gallery' => array(
            'label' => array(
                'de' => array('Galerie', 'Container ohne Galerie als Standardeinstellung'),
            ),
            'inputType' => 'checkbox',
        ),

        ### gallery ###
        'gallery' => array(
            'label' => array('Galerie', 'Hier können Sie Bilder auswählen'),
            'inputType' => 'fileTree',
            'eval' => array(
                'multiple' => true,
                'isGallery' => true,
                'tl_class' => 'clr autoheight',
                'fieldType' => 'checkbox',
                'filesOnly' => true,
                'orderField'=>'orderSRC',
                'extensions' => \Config::get('validImageTypes'),
            ),
            'load_callback' => array
            (
                array('tl_content', 'setMultiSrcFlags')
            ),
            'dependsOn' => [
                'field' => 'checkbox_gallery',
            ],
        ),
    ),
);
