<?php
#####################################
### Created by S Punkt Online #######
### https://www.s-punkt-online.de ###
#####################################

    return array(
        'label' => array(
            'de' => array('Liste von Leistungen: Bild, Beschreibung, Link/Button', ''),
        ),
        'types' => array('content'),
        'contentCategory' => 'RSCE: Inhalt',
        //'beTemplate' => 'be_wildcard',
        'standardFields' => array('headline', 'cssID'),
        'wrapper' => array(
            'type' => 'none',
        ),
        

        'fields' => array(

            // TEXT;
            'subtitle' => array(
                'label' => array(
                    'de' => array('Untertitel', 'Hier können Sie einen Text hinzufügen'),
                ),
                'inputType' => 'text',
                'eval' => array('tl_class' => 'clr w50'),
            ),

            'checkbox_list' => array(
                'label' => array('Erscheinungsbild', 'die gewünschten Elemente auswählen, die auf der Seite angezeigt werden sollen'),
                'inputType' => 'group',
            ),
            'chb_background' => array(
                'label' => array(
                    'de' => array('Hinzufügen einer Hintergrundfarbe für den Block', 'Block ohne Hintergrundfarbe als Standard'),
                ),
                'inputType' => 'checkbox',
            ),
            'chb_image' => array(
                'label' => array(
                    'de' => array('Bilder 75px breit', '100% als Standard'),
                ),
                'inputType' => 'checkbox',
            ),

            'checkbox_title_decoration' => array(
                'label' => array(
                    'de' => array('Stilisiertes Kästchen für die Titel', 'Elemente ohne Stilisierung als Standardeinstellung'),
                ),
                'inputType' => 'checkbox',
            ),
            'servicesList' => array(
                'label' => array(
                    'de' => array('Liste der Leistungen ', 'Hier können Sie Leistungen ergänzen'),
                ),
                'elementLabel' => array(
                    'de' => 'Leistung %s',
                ),

                'inputType' => 'list',
                'fields' => array(
                    'servicesItemIcon' => array(
                        'label' => array(
                            'de' => array('Bild/-Icon', 'Hier können Sie ein Icon/Bild für die Leistung auswählen'),
                        ),
                        'inputType' => 'fileTree',
                        'eval' => array(
                            'fieldType' => 'radio',
                            'filesOnly' => true,
                            'extensions' => 'jpg,jpeg,png,gif,svg',
                        ),
                    ),

                    'image_alt' => array(
                        'label' => array(
                            'de' => array('Alt-Attribute', 'Hier können Sie einen Alt-Attribute für das Bild eingeben'),
                        ),
                        'inputType' => 'text',
                        'eval' => array(
                            'tl_class' => 'w50 clr',
                            'mandatory' => true,
                        ),
                    ),
                    'image_title' => array(
                        'label' => array(
                            'de' => array('Bildtitel', 'Hier können Sie den Titel des Bildes eingeben (title-Attribut)'),
                        ),
                        'inputType' => 'text',
                        'eval' => array(
                            'tl_class' => 'w50 clr',
                            'mandatory' => true,
                        ),
                    ),
                    'servicesItemTitle' => array(
                        'label' => array(
                            'de' => array('Überschrift', 'Hier können Sie die Überschrift für die Leistung einfügen'),
                        ),
                        'inputType' => 'inputUnit',
                        'options' => array('h3', 'h4', 'h5', 'h6'),
                        'eval' => array('tl_class' => 'w50 clr'),
                    ),

                    'servicesItemDescription' => array(
                        'label' => array(
                            'de' => array('Text', 'Hier können Sie den Text für die Leistung einfügen'),
                        ),
                        'inputType' => 'textarea',
                        'eval' => array(
                            'tl_class' => 'clr',
                            'rte' => 'tinyMCE'
                        ),
                    ),
                    /*** CHECKBOX ***/
                    'checkbox' => array(
                        'label' => array(
                            'de' => array('Link zeigen', 'Block ohne Link als Standardeinstellung'),
                        ),
                        'inputType' => 'checkbox',
                        'eval' => array(
                            'tl_class' => 'w50',
                        ),
                    ),
                    'link_radio' => array(
                        'label' => array('Erscheinungsbild für Links'),
                        'inputType' => 'radio',
                        'default' => 'link',
                        'options' => array(
                            'link' => 'als Link',
                            'button' => 'als Button',
                        ),
                        'eval' => array(
                            'tl_class' => 'w50',
                        ),
                    ),
                    'servicesLink' => array(
                        'label' => array(
                            'de' => array('Link', 'Hier können Sie das Link auswählen'),
                            'en' => array('Link'),
                        ),
                        'inputType' => 'url',
                        'eval' => array('tl_class' => 'w50 clr'),
                    ),
                    'servicesLinkIcon' => array(
                        'label' => array(
                            'de' => array('Icon', 'Hier können Sie das Bild auswählen'),
                        ),
                        'inputType' => 'fileTree',
                        'eval' => array(
                            'fieldType' => 'radio',
                            'filesOnly' => true,
                            'extensions' => 'jpg,jpeg,png,gif,svg',
                            'tl_class'=>'w50',
                        ),
                    ),
                    'servicesLinkText' => array(
                        'label' => array(
                            'de' => array('Link Text', 'Hier können Sie einen Text hinzufügen'),
                            'en' => array('Link-Text'),
                        ),
                        'inputType' => 'text',
                        'eval' => array('tl_class' => 'w50'),
                    ),
                    'servicesLinkTitle' => array(
                        'label' => array(
                            'de' => array('Link-Title', 'Hier können Sie einen Text hinzufügen'),
                            'en' => array('Link-Text'),
                        ),
                        'inputType' => 'text',
                        'eval' => array('tl_class' => 'w50'),
                    ),
                ),
            ),
        ),

    );