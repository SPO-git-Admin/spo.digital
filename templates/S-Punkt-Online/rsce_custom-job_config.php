<?php
return array(
    'label' => array(
        'de' => array('Stellenausschreibung', ''),
    ),
    'types' => array('content'),
    'contentCategory' => 'RSCE: Karriere',
    //'beTemplate' => 'be_wildcard',
    'standardFields' => array('headline', 'cssID'),
    'wrapper' => array(
        'type' => 'none',
    ),
    //'moduleCategory' => 'miscellaneous',

    'fields' => array(
        'description' => array(
            'label' => array('Beschreibung', 'Ausführliche Beschreibung der Stelle'),
            'eval' => array('rte' => 'tinyMCE'),
            'inputType' => 'textarea',
        ),
        'list_description' => array(
            'label' => array('Beschreibung für Unten', 'Ausführliche Beschreibung der Stelle'),
            'eval' => array('rte' => 'tinyMCE'),
            'inputType' => 'textarea',
        ),
        'logo' => array(
            'label' => array(
                'de' => array('Logo', 'Hier können Sie das Bild auswählen'),
            ),
            'inputType' => 'fileTree',
            'eval' => array(
                'fieldType' => 'radio',
                'filesOnly' => true,
                'extensions' => 'jpg,jpeg,png,gif,svg',
                'tl_class'=>'w50',
            ),
        ),
        'image' => array(
            'label' => array(
                'de' => array('Bild', 'Hier können Sie das Bild auswählen'),
            ),
            'inputType' => 'fileTree',
            'eval' => array(
                'fieldType' => 'radio',
                'filesOnly' => true,
                'extensions' => 'jpg,jpeg,png,gif,svg',
                'tl_class'=>'w50',
            ),
        ),
        'hiringOrganization' => array(
            'label' => array('Arbeitgeber', 'Arbeitgebername'),
            'inputType' => 'text',
            'eval' => array('mandatory'=>true, 'tl_class'=>'w50 clr'),
        ),
        'jobLocationStreet' => array(
            'label' => array('Strasse', 'Strasse der Hauptarbeitsstätte'),
            'inputType' => 'text',
            'eval' => array('mandatory'=>true, 'tl_class'=>'w50 clr'),
        ),
        'jobLocationPostal' => array(
            'label' => array('PLZ', 'PLZ der Hauptarbeitsstätte'),
            'inputType' => 'text',
            'eval' => array('mandatory'=>true, 'tl_class'=>'w50 clr'),
        ),
        'jobLocationLocality' => array(
            'label' => array('Ort', 'Ort der Hauptarbeitsstätte'),
            'inputType' => 'text',
            'eval' => array('mandatory'=>true, 'tl_class'=>'w50 clr'),
        ),
        'employmentType' => array(
            'label' => array('Beschäftigungsart', 'Bitte wählen Sie die Art der Beschäftigung aus'),
            'inputType' => 'select',
            'eval' => array('mandatory'=>true, 'tl_class'=>'w50 clr'),
            'default' => 'vollzeit',
            'options' => array(
                'Teilzeit' => 'Teilzeit',
                'Vollzeit' => 'Vollzeit',
                'Ausbildung' => 'Ausbildung',
                'Geringfügige Beschäftigung' => 'Geringfügige Beschäftigung',
                'Freie Mitarbeit' => 'Freie Mitarbeit',
                'Saisonal' => 'Saisonal',
            ),
        ),
        'downloadFile' => array(
            'label' => array('Download-Elemente', 'Zusätzliche Dokumente zur Stellenausschreibung können hier ausgewählt werden.'),
            'inputType' => 'fileTree',
            'eval' => array(
                'fieldType' => 'radio',
                'filesOnly' => true,
                'extensions' => 'jpg,jpeg,png,pdf',
                'tl_class'=>'w50 clr',
            ),
        ),
        'downloadName' => array(
            'label' => array('Downloadbezeichnung', 'Diese Bezeichnung wird anstatt dem Dateinamen angezeigt'),
            'inputType' => 'text',
            'eval' => array('tl_class'=>'w50'),
        ),
    ),
);