<?php
#####################################
### Created by S Punkt Online #######
### https://www.s-punkt-online.de ###
#####################################

return array (
    'label' => array( 'Galerie: Mosaic-Design' ),
    'types' => array( 'content' ),
    'contentCategory' => 'RSCE: Referenzen',
    'standardFields' => array( 'headline', 'cssID' ),
    'wrapper' => array(
        'type' => 'none',
    ),

    'fields' => array(

        ### group ###
        'hex_group' => array(
            'label' => array( 'Gradient-Farbe', 'Hier können Sie Gradient hinzufügen.' ),
            'inputType' => 'group',
        ),

        ### text ###
        'hex' => array(
            'label' => array(
                'de' => array('Gradient-Farbe von:', 'Hier können Sie HEX ohne # eingeben'),
            ),
            'inputType' => 'text',
            'eval' => array(
                'tl_class' => 'w50 clr',
                'mandatory' => true
            ),
        ),

        ### text ###
        'hex_1' => array(
            'label' => array(
                'de' => array('Gradient-Farbe bis:', 'Hier können Sie HEX ohne # eingeben'),
            ),
            'inputType' => 'text',
            'eval' => array(
                'tl_class' => 'w50',
                'mandatory' => true
            ),
        ),

        ### group ###
        'gallery_group' => array(
            'label' => array( 'Galerie', 'Hier können Sie eine Galerie hinzufügen.' ),
            'inputType' => 'group',
        ),

        ### gallery ###
        'gallery' => array(
            'label' => array('Galerie', 'Hier können Sie Bilder auswählen'),
            'inputType' => 'fileTree',
            'eval' => array(
                'multiple' => true,
                'isGallery' => true,
                'tl_class' => 'clr autoheight',
                'fieldType' => 'checkbox',
                'filesOnly' => true,
                'orderField'=>'orderSRC',
                'extensions' => \Config::get('validImageTypes'),
            ),
            'load_callback' => array
            (
                array('tl_content', 'setMultiSrcFlags')
            )
        ),

    ),
);
