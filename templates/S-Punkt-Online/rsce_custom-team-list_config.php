<?php
return array(
    'label' => array(
        'de' => array('Team-Liste: Eine strukturierte Vorlage für Unterseite Team', ''),
    ),

    'types' => array('content'),
    'contentCategory' => 'RSCE: Inhalt',
    //'beTemplate' => 'be_wildcard',
    'standardFields' => array('headline', 'cssID'),
    'wrapper' => array(
        'type' => 'none',
    ),

    'fields' => array(
        'list' => array(
            'label' => array(
                'de' => array('Team-Liste ', 'Hier können Sie die Anzahl der Mitarbeiter festlegen'),
            ),
            'elementLabel' => array(
                'de' => 'Team-Item %s',
            ),

            'inputType' => 'list',
            'fields' => array(
                'image' => array(
                    'label' => array(
                        'de' => array('Profilbild', 'Hier können Sie das Bild auswählen'),
                    ),
                    'inputType' => 'fileTree',
                    'eval' => array(
                        'fieldType' => 'radio',
                        'filesOnly' => true,
                        'extensions' => 'jpg,jpeg,png,gif,svg,webp',
                        'tl_class'=>'w50',
                    ),
                ),
                'memberName' => array(
                    'label' => array(
                        'de' => array('Vor- und Nachname', 'Hier können Sie Vor- und Nachname des Mitarbeiters hinzufügen'),
                    ),
                    'inputType' => 'text',
                    'eval' => array('tl_class' => 'w50 clr'),
                ),
                'memberTitle' => array(
                    'label' => array(
                        'de' => array('Position', 'Hier können Sie die Position des Mitarbeiters hinzufügen'),
                    ),
                    'inputType' => 'text',
                    'eval' => array('tl_class' => 'w50'),
                ),
                'contacts' => array(
                    'label' => array(
                        'de' => array(
                            'Kontakten',
                            'Fügen Sie eine beliebige Anzahl an Kontakten ein.',
                        ),
                    ),
                    'elementLabel' => array(
                        'de' => '%s. Social-Media-Profil',
                        'en' => '%s. social media profile',
                    ),
                    'inputType' => 'list',
                    'fields' => array(
                        'contactsImage' => array(
                            'label' => array(
                                'de' => array('Icon', 'Hier können Sie das Bild auswählen'),
                            ),
                            'inputType' => 'fileTree',
                            'eval' => array(
                                'fieldType' => 'radio',
                                'filesOnly' => true,
                                'extensions' => 'jpg,jpeg,png,gif,svg',
                                'tl_class'=>'w50',
                            ),
                        ),
                        'url' => array(
                            'label' => array(
                                'de' => array('Kontakt', 'Telefonnumen, Link, Email'),
                            ),
                            'inputType' => 'text',
                            'eval' => array('tl_class' => 'w50'),
                        ),
                    ),
                ),
                'memberContactsDescription' =>  array(
                    'label' => array(
                        'de' => array('Beschreibung von den Kontakten'),
                        'en' => array('Description'),
                    ),
                    'inputType' => 'textarea',
                    'eval' => array('rte' => 'tinyMCE'),
                ),
                'link' => array(
                    'label' => array(
                        'de' => array('Link', 'Hier können Sie ein Link auswählen'),
                    ),
                    'inputType' => 'url',
                    'eval' => array('tl_class' => 'w50'),
                ),

                'linkText' => array(
                    'label' => array(
                        'de' => array('Link-Text', 'Hier können Sie den Text für Button hinzufügen'),
                    ),
                    'inputType' => 'text',
                    'eval' => array('tl_class' => 'w50'),
                ),
                'link_icon' => array(
                    'label' => array(
                        'de' => array('Icon', 'Hier können Sie das Bild auswählen'),
                    ),
                    'inputType' => 'fileTree',
                    'eval' => array(
                        'fieldType' => 'radio',
                        'filesOnly' => true,
                        'extensions' => 'jpg,jpeg,png,gif,svg',
                        'tl_class'=>'w50',
                    ),
                ),

            ),
        ),
    ),
);