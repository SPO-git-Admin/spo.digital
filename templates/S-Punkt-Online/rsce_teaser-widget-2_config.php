<?php
return array(
    'label' => array(
        'de' => array(
            'Teaser-Widget: Text rechts, Bild links',
            'Bild mit einem Text und Link',
        ),
        'en' => array(
            'Custom: Teaser-Widget (Text left, Image right)',
            'Image with Text and Link',
        ),
    ),

    'types' => array('content'),
    'contentCategory' => 'RSCE: Inhalt',
    //'beTemplate' => 'be_wildcard',
    'standardFields' => array('cssID'),
    'wrapper' => array(
        'type' => 'none',
    ),
    'fields' => array(
        // GROUP
        'image_grp' => array(
            'label' => array('Bild-Gruppe', 'Fügen Sie ein Bild, einen Titel und eine ALT-Attribute ein.'),
            'inputType' => 'group',
        ),

        // IMAGE
        'image' => array(
            'label' => array(
                'de' => array('Hintergrundbild', 'Hier können Sie das Bild auswählen'),
                'en' => array('Background-Image'),
            ),
            'inputType' => 'fileTree',
            'eval' => array(
                'fieldType' => 'radio',
                'filesOnly' => true,
                'extensions' => 'jpg,jpeg,png,gif,svg',
            ),
        ),

        // TEXT
        'image_alt' => array(
            'label' => array(
                'de' => array('Alt-Attribute', 'Hier können Sie einen Alt-Attribute für das Bild eingeben'),
            ),
            'inputType' => 'text',
            'eval' => array(
                'tl_class' => 'w50 clr',
                'mandatory' => true,
            ),
        ),

        // TEXT
        'image_title' => array(
            'label' => array(
                'de' => array('Bildtitel', 'Hier können Sie den Titel des Bildes eingeben (title-Attribut)'),
            ),
            'inputType' => 'text',
            'eval' => array(
                'tl_class' => 'w50',
                'mandatory' => true,
            ),
        ),

        // GROUP
        'text_grp' => array(
            'label' => array('Text-Gruppe', 'Fügen Sie einen Titel und eine kürze Beschreibung ein.'),
            'inputType' => 'group',
        ),

        'title' => array(
            'label' => array(
                'de' => array('Überschrift', 'Hier können Sie einen Text einfügenein'),
            ),

            'inputType' => 'text',
            'eval' => array('tl_class' => 'w50'),
        ),

        'description' =>  array(
            'label' => array(
                'de' => array('Beschreibung', 'Hier können Sie einen Text einfügenein'),
            ),
            'inputType' => 'text',
            'eval' => array('tl_class' => 'w50'),
        ),

        // GROUP
        'button_grp' => array(
            'label' => array('Link-Gruppe', 'Fügen Sie einen Link ein.'),
            'inputType' => 'group',
        ),

        'link' => array(
            'label' => array(
                'de' => array('Button-Link', 'Hier können Sie ein Link auswählen'),
                'en' => array('Button-Link'),
            ),
            'inputType' => 'url',
            'eval' => array('tl_class' => 'w50'),
        ),

        'linkText' => array(
            'label' => array(
                'de' => array('Button-Text', 'Hier können Sie den Text für Button schreiben'),
                'en' => array('Button-Text'),
            ),
            'inputType' => 'text',
            'eval' => array('tl_class' => 'w50'),
        ),

        'link_icon' => array(
            'label' => array(
                'de' => array('Icon', 'Hier können Sie das Bild auswählen'),
            ),
            'inputType' => 'fileTree',
            'eval' => array(
                'fieldType' => 'radio',
                'filesOnly' => true,
                'extensions' => 'jpg,jpeg,png,gif,svg',
                'tl_class'=>'w50',
            ),
        ),

        'css' => array(
            'label' => array(
                'de' => array('CSS-Klasse'),
            ),
            'inputType' => 'text',
            'eval' => array('tl_class'=>'w50 clr'),
        ),

    ),
);
