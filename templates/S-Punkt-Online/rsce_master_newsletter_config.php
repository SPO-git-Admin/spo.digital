<?php
return array(
    'label' => array(
        'de' => array('Footer: Newsletter', 'Eine strukturierte Vorlage für Newsletter Block'),
    ),

    'types' => array('content'),
    //'contentCategory' => 'RSCE: NODE',
    'beTemplate' => 'be_wildcard',
    'standardFields' => array('cssID'),
    'wrapper' => array(
        'type' => 'none',
    ),
    
    'fields' => array(
        'tel' => array(
            'label' => array('Telefonnummer', 'Hier können Sie die Telefonnumer hinzufügen'),
            'inputType' => 'text',
            'eval' => array('mandatory'=>true, 'tl_class'=>'w50 clr'),
        ),
        'tel_icon' => array(
            'label' => array(
                'de' => array('Icon', 'Hier können Sie das Bild auswählen'),
            ),
            'inputType' => 'fileTree',
            'eval' => array(
                'fieldType' => 'radio',
                'filesOnly' => true,
                'extensions' => 'jpg,jpeg,png,gif,svg',
                'tl_class'=>'w50',
                'mandatory'=>true,
            ),
        ),
        'link' => array(
            'label' => array(
                'de' => array('Link', 'Hier können Sie ein Link auswählen'),
            ),
            'inputType' => 'url',
            'eval' => array('tl_class' => 'w50 clr', 'mandatory'=>true),

        ),
        'link_title' => array(
            'label' => array(
                'de' => array('Link-Title', 'Hier können Sie den Titel für den Link hinzufügen'),
            ),
            'inputType' => 'text',
            'eval' => array(
                'tl_class' => 'w50',
                'mandatory' => true,
            ),
        ),
        'link_text' => array(
            'label' => array(
                'de' => array('Link-Text', 'Hier können Sie den Text für Button hinzufügen'),
            ),
            'inputType' => 'text',
            'eval' => array('tl_class' => 'w50 clr', 'mandatory'=>true),
        ),
        'link_icon' => array(
            'label' => array(
                'de' => array('Icon', 'Hier können Sie das Bild auswählen'),
            ),
            'inputType' => 'fileTree',
            'eval' => array(
                'fieldType' => 'radio',
                'filesOnly' => true,
                'extensions' => 'jpg,jpeg,png,gif,svg',
                'tl_class'=>'w50',
                'mandatory'=>true,
            ),
        ),
    ),
);