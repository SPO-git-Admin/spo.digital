<?php
return array(
    'label' => array(
        'de' => array('Team-Preview: Eine strukturierte Vorlage für Team-Preview-Widget', ''),
    ),

    'types' => array('content'),
    'contentCategory' => 'RSCE: Inhalt',
    //'beTemplate' => 'be_wildcard',
    'standardFields' => array('headline', 'cssID'),
    'wrapper' => array(
        'type' => 'none',
    ),
    'fields' => array(
        'team' => array(
            'label' => array(
                'de' => array(
                    'Team-Preview-List',
                    'Fügen Sie eine beliebige Anzahl an Mitarbeiter ein.',
                ),
            ),
            'elementLabel' => array(
                'de' => '%s. Mitarbeiter',
            ),
            'inputType' => 'list',
            'fields' => array(
                'image' => array(
                    'label' => array(
                        'de' => array('Bild', 'Hier können Sie das Bild auswählen'),
                    ),
                    'inputType' => 'fileTree',
                    'eval' => array(
                        'fieldType' => 'radio',
                        'filesOnly' => true,
                        'extensions' => 'jpg,jpeg,png,gif,svg',
                        'tl_class'=>'w50',
                    ),
                ),
                'name' => array(
                    'label' => array('Vorname und Nachname', 'Hier können Sie Vorname und Nachname hinzufügen'),
                    'inputType' => 'text',
                    'eval' => array('mandatory'=>true, 'tl_class'=>'w50 clr'),
                ),
                'link' => array(
                    'label' => array(
                        'de' => array('Link', 'Hier können Sie ein Link auswählen'),
                    ),
                    'inputType' => 'url',
                    'eval' => array('tl_class' => 'w50'),
                ),
            ),
        ),
    ),
);