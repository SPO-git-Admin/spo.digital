<?php
#####################################
### Created by S Punkt Online #######
### https://www.s-punkt-online.de ###
#####################################

return array(
    'label' => array(
        'de' => array('Profil-Seite: Vorlage für eine Mitarbeiterprofilseite', ''),
    ),
    'types' => array('content'),
    'contentCategory' => 'RSCE: Inhalt',
    'beTemplate' => 'be_wildcard',
    'standardFields' => array('headline','cssID'),
    'wrapper' => array(
        'type' => 'none',
    ),

    'fields' => array(

        ### group ###
        'checkbox_group' => array(
            'label' => array('Erscheinungsbild', 'die gewünschten Elemente auswählen, die auf der Seite angezeigt werden sollen'),
            'inputType' => 'group',
        ),

        ### group ###
        'description_group' => array(
            'label' => array('Beschreibung', 'Hier können Sie eine Beschreibung und ein Bild hinzufügen'),
            'inputType' => 'group',
        ),
        ### image ###
        'avatar' => array(
            'label' => array(
                'de' => array('Quelldatei: (230px * 265px)', 'Hier können Sie das Bild auswählen'),
            ),
            'inputType' => 'fileTree',
            'eval' => array(
                'fieldType' => 'radio',
                'filesOnly' => true,
                'extensions' => 'jpg,jpeg,png,gif,svg,webp',
                'tl_class'=>'w50 clr',
                'mandatory' => true,
            ),
        ),
        'avatar_alt' => array(
            'label' => array(
                'de' => array('Alt-Attribute', 'Hier können Sie einen Alt-Attribute für das Bild eingeben'),
            ),
            'inputType' => 'text',
            'eval' => array(
                'tl_class' => 'w50 clr',
                'mandatory' => true
            ),
        ),
        'avatar_title' => array(
            'label' => array(
                'de' => array('Bildtitel', 'Hier können Sie den Titel des Bildes eingeben (title-Attribut)'),
            ),
            'inputType' => 'text',
            'eval' => array(
                'tl_class' => 'w50',
                'mandatory' => true
            ),
        ),

        // TEXT;
        'role' => array(
            'label' => array(
                'de' => array('Die Position', 'Hier können Sie einen Text hinzufügen'),
            ),
            'inputType' => 'text',
            'eval' => array('tl_class' => 'clr'),
        ),

        ### textarea ###
        'description' => array(
            'label' => array('Die Beschreibung', 'Hier können Sie die Beschreibung hinzufügen'),
            'eval' => array('rte' => 'tinyMCE'),
            'inputType' => 'textarea',
            'tl_class' => 'clr',
        ),



        ### list ###
        'contacts_list' => array(
            'label' => array('Kontaktliste mit Icons', 'Fügen Sie Elemente hinzu.'),
            'elementLabel' => '%s. Kontakt',
            'inputType' => 'list',
            'minItems' => 1,
            'fields' => array(
                ### link ###
                'contact' => array(
                    'label' => array(
                        'de' => array('Link', 'Hier können Sie das Link auswählen'),
                        'en' => array('Link'),
                    ),
                    'inputType' => 'url',
                    'eval' => array(
                        'tl_class' => 'w50 clr',
                        'mandatory' => true
                    ),
                ),

                // IMAGE;
                'contact_icon' => array(
                    'label' => array(
                        'de' => array('Icon', 'Hier können Sie das Bild auswählen'),
                    ),
                    'inputType' => 'fileTree',
                    'eval' => array(
                        'fieldType' => 'radio',
                        'filesOnly' => true,
                        'extensions' => 'jpg,jpeg,png,gif,svg',
                        'tl_class'=>'w50',
                        'mandatory' => true
                    ),
                ),

                // TEXT;
                'contact_text' => array(
                    'label' => array(
                        'de' => array('Text', 'Hier können Sie einen Text hinzufügen'),
                        'en' => array('Link-Text'),
                    ),
                    'inputType' => 'text',
                    'eval' => array(
                        'tl_class' => 'w50',
                        'mandatory' => true
                    ),
                ),

                // TEXT;
                'contact_title' => array(
                    'label' => array(
                        'de' => array('Link-Title', 'Hier können Sie einen Text hinzufügen'),
                        'en' => array('Link-Title'),
                    ),
                    'inputType' => 'text',
                    'eval' => array(
                        'tl_class' => 'w50',
                        'mandatory' => true
                    ),
                ),
            ),
        ),
        ### list ###
        'sml_list' => array(
            'label' => array('SML-Liste mit Icons', 'Fügen Sie Elemente hinzu.'),
            'elementLabel' => '%s. SML',
            'inputType' => 'list',
            'minItems' => 1,
            'fields' => array(
                // LINK;
                'sml' => array(
                    'label' => array(
                        'de' => array('Link', 'Hier können Sie das Link auswählen'),
                        'en' => array('Link'),
                    ),
                    'inputType' => 'url',
                    'eval' => array(
                        'tl_class' => 'w50 clr',
                        'mandatory' => true
                    ),
                ),

                // IMAGE;
                'sml_icon' => array(
                    'label' => array(
                        'de' => array('Icon', 'Hier können Sie das Bild auswählen'),
                    ),
                    'inputType' => 'fileTree',
                    'eval' => array(
                        'fieldType' => 'radio',
                        'filesOnly' => true,
                        'extensions' => 'jpg,jpeg,png,gif,svg',
                        'tl_class'=>'w50',
                        'mandatory' => true
                    ),
                ),

                // TEXT;
                'sml_title' => array(
                    'label' => array(
                        'de' => array('Link-Title', 'Hier können Sie einen Text hinzufügen'),
                        'en' => array('Link-Title'),
                    ),
                    'inputType' => 'text',
                    'eval' => array(
                        'tl_class' => 'w50',
                        'mandatory' => true
                    ),
                ),
            ),
        ),
        ### group ###
        'deco_image_group' => array(
            'label' => array('Decoration-Bild', 'Hier können Sie ein Bild hinzufügen'),
            'inputType' => 'group',
        ),
        ### image ###
        'deco_image' => array(
            'label' => array(
                'de' => array('Quelldatei: (433px * 348px)', 'Hier können Sie das Bild auswählen'),
            ),
            'inputType' => 'fileTree',
            'eval' => array(
                'fieldType' => 'radio',
                'filesOnly' => true,
                'extensions' => 'jpg,jpeg,png,gif,svg,webp',
                'tl_class'=>'w50 clr',
                'mandatory' => true,
            ),
        ),
        'deco_image_alt' => array(
            'label' => array(
                'de' => array('Alt-Attribute', 'Hier können Sie einen Alt-Attribute für das Bild eingeben'),
            ),
            'inputType' => 'text',
            'eval' => array(
                'tl_class' => 'w50 clr',
                'mandatory' => true
            ),
        ),
        'deco_image_title' => array(
            'label' => array(
                'de' => array('Bildtitel', 'Hier können Sie den Titel des Bildes eingeben (title-Attribut)'),
            ),
            'inputType' => 'text',
            'eval' => array(
                'tl_class' => 'w50 clr',
                'mandatory' => true
            ),
        ),
        ### group ###
        'questions_group' => array(
            'label' => array('Fragen und Antworten', 'Hier können Sie eine Überschrift, Fragen und Antworten hinzufügen'),
            'inputType' => 'group',
        ),
        ### title ###
        'questions_title' => array(
            'label' => array(
                'de' => array('Überschrift', 'Hier können Sie die Überschrift für den Fragen-Block hinzufügen'),
            ),
            'inputType' => 'inputUnit',
            'options' => array('h2','h3', 'h4', 'h5', 'h6'),
            'eval' => array('tl_class' => 'w50 clr'),
        ),
        ### list ###
        'questions_list' => array(
            'label' => array('Liste der Fragen', 'Fügen Sie Elemente hinzu.'),
            'elementLabel' => '%s. Frage',
            'inputType' => 'list',
            'minItems' => 1,
            'fields' => array(
                'question' => array(
                    'label' => array(
                        'de' => array('Die Frage', 'Hier können Sie die Frage hinzufügen'),
                    ),
                    'inputType' => 'inputUnit',
                    'options' => array('h3', 'h4', 'h5', 'h6'),
                    'eval' => array('tl_class' => 'clr'),
                ),
                ### textarea ###
                'answer' => array(
                    'label' => array('Die Antwort', 'Hier können Sie die Antwort hinzufügen'),
                    'eval' => array('rte' => 'tinyMCE'),
                    'inputType' => 'textarea',
                    'tl_class' => 'clr',
                ),
            ),
        ),
        ### group ###
        'education_group' => array(
            'label' => array('Kurzvita', 'Hier können Sie einen Text hinzufügen'),
            'inputType' => 'group',
        ),
        'education_list' => array(
            'label' => array('Die Bildung', 'Hier können Sie die Antwort hinzufügen'),
            'eval' => array('rte' => 'tinyMCE'),
            'inputType' => 'textarea',
            'tl_class' => 'clr',
        ),
    ),
);
