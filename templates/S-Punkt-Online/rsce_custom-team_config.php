<?php
return array(
    'label' => array(
        'de' => array('Custom: Unser Team', 'Mitarbeiter Liste'),
    ),
    'types' => array('content'),
    'standardFields' => array('headline','cssId'),

    'fields' => array(
        'css' => array(
            'label' => array(
                'de' => array('CSS-Klasse', 'Hier können Sie beliebig viele Klassen eingeben'),
            ),
            'inputType' => 'text',
            'eval' => array('tl_class'=>'w50'),
        ),
        'mainLink' => array(
            'label' => array(
                'de' => array('Link', 'Hier können Sie ein Link auswählen'),
            ),
            'inputType' => 'url',
            'eval' => array('tl_class' => 'w50 clr'),
        ),

        'mainLinkText' => array(
            'label' => array(
                'de' => array('Link-Text', 'Hier können Sie den Text für Button hinzufügen'),
            ),
            'inputType' => 'text',
            'eval' => array('tl_class' => 'w50'),
        ),

        'members' => array(
            'label' => array(
                'de' => array(
                    'Personen',
                    'Fügen Sie eine beliebige Anzahl an Mitarbeitern ein.',
                ),
            ),
            'elementLabel' => array(
                'de' => 'Mitarbeiter %s',
            ),
            'inputType' => 'list',
            'fields' => array(
                'profileImage' => array(
                    'label' => array(
                        'de' => array('Profilbild', 'Hier können Sie das Bild auswählen'),
                    ),
                    'inputType' => 'fileTree',
                    'eval' => array(
                        'fieldType' => 'radio',
                        'filesOnly' => true,
                        'extensions' => 'jpg,jpeg,png,gif,svg,webp',
                        'tl_class'=>'w50',
                    ),
                ),
                'placeholderImage' => array(
                    'label' => array(
                        'de' => array('Gemaltes Bild', 'Hier können Sie das Bild auswählen'),
                    ),
                    'inputType' => 'fileTree',
                    'eval' => array(
                        'fieldType' => 'radio',
                        'filesOnly' => true,
                        'extensions' => 'jpg,jpeg,png,gif,svg,webp',
                        'tl_class'=>'w50',
                    ),
                ),
                'memberName' => array(
                    'label' => array(
                        'de' => array('Vor- und Nachname', 'Hier können Sie Vor- und Nachname des Mitarbeiters hinzufügen'),
                    ),
                    'inputType' => 'text',
                    'eval' => array('tl_class' => 'w50'),
                ),
                'memberTitle' => array(
                    'label' => array(
                        'de' => array('Position', 'Hier können Sie die Position des Mitarbeiters hinzufügen'),
                    ),
                    'inputType' => 'text',
                    'eval' => array('tl_class' => 'w50'),
                ),
                'contacts' => array(
                    'label' => array(
                        'de' => array(
                            'Kontakten',
                            'Fügen Sie eine beliebige Anzahl an Kontakten ein.',
                        ),
                    ),
                    'elementLabel' => array(
                        'de' => '%s. Social-Media-Profil',
                        'en' => '%s. social media profile',
                    ),
                    'inputType' => 'list',
                    'fields' => array(
                        'contactsImage' => array(
                            'label' => array(
                                'de' => array('Icon', 'Hier können Sie das Bild auswählen'),
                            ),
                            'inputType' => 'fileTree',
                            'eval' => array(
                                'fieldType' => 'radio',
                                'filesOnly' => true,
                                'extensions' => 'jpg,jpeg,png,gif,svg,webp',
                                'tl_class'=>'w50',
                            ),
                        ),
                        'url' => array(
                            'label' => array(
                                'de' => array('Kontakt', 'Telefonnumen, Link, Email'),
                            ),
                            'inputType' => 'text',
                            'eval' => array('tl_class' => 'w50'),
                        ),
                    ),
                ),
                'memberContactsDescription' =>  array(
                    'label' => array(
                        'de' => array('Beschreibung von den Kontakten'),
                        'en' => array('Description'),
                    ),
                    'inputType' => 'textarea',
                    'eval' => array('rte' => 'tinyMCE'),
                ),
                'memberDescription' =>  array(
                    'label' => array(
                        'de' => array('Beschreibung vom Mitarbeiter/in'),
                        'en' => array('Description'),
                    ),
                    'inputType' => 'textarea',
                    'eval' => array('rte' => 'tinyMCE'),
                ),
            ),
        ),
        'jobMembers' => array(
            'label' => array(
                'de' => array(
                    'Aktuelle Job-Stellen',
                    'Fügen Sie eine beliebige Anzahl an Job-Stellen ein.',
                ),
            ),
            'elementLabel' => array(
                'de' => 'Job-Stelle %s',
            ),
            'inputType' => 'list',
            'fields' => array(
                'jobImage' => array(
                    'label' => array(
                        'de' => array('Bild von der Stelle', 'Hier können Sie das Bild auswählen'),
                    ),
                    'inputType' => 'fileTree',
                    'eval' => array(
                        'fieldType' => 'radio',
                        'filesOnly' => true,
                        'extensions' => 'jpg,jpeg,png,gif,svg,webp',
                    ),
                ),
                'jobTitle' => array(
                    'label' => array(
                        'de' => array('Überschrift von der Stelle', 'Hier können Sie Überschrift von der Stelle hinzufügen'),
                    ),
                    'inputType' => 'text',
                ),
                'jobDescription' =>  array(
                    'label' => array(
                        'de' => array('Beschreibung von der Stelle', 'Hier können Sie eine Beschreibung von der Stelle hinzufügen'),
                    ),
                    'inputType' => 'textarea',
                    'eval' => array(
                        'rte' => 'tinyMCE',
                    ),
                ),
                'jobLink' => array(
                    'label' => array(
                        'de' => array('Stelle-Link', 'Hier können Sie ein Link auswählen'),
                    ),
                    'inputType' => 'url',
                    'eval' => array('tl_class' => 'w50'),
                ),

                'jobLinkText' => array(
                    'label' => array(
                        'de' => array('Stelle-Link-Text', 'Hier können Sie den Text für Button hinzufügen'),
                    ),
                    'inputType' => 'text',
                    'eval' => array('tl_class' => 'w50'),
                ),
            ),
        ),
    ),
);