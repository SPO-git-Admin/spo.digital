<?php
return array(
    'label' => array(
        'de' => array('Detaillierte Teamseite: Avatar, Beschreibung, SML', ''),
    ),
    'types' => array('content'),
    'contentCategory' => 'RSCE: Inhalt',
    //'beTemplate' => 'be_wildcard',
    'standardFields' => array('headline', 'cssID'),
    'wrapper' => array(
        'type' => 'none',
    ),
    'fields' => array(
        'profileImage' => array(
            'label' => array(
                'de' => array('Profilbild', 'Hier können Sie das Bild auswählen'),
            ),
            'inputType' => 'fileTree',
            'eval' => array(
                'fieldType' => 'radio',
                'filesOnly' => true,
                'extensions' => 'jpg,jpeg,png,gif,svg',
                'tl_class'=>'w50',
            ),
        ),
        'memberName' => array(
            'label' => array(
                'de' => array('Vor- und Nachname', 'Hier können Sie Vor- und Nachname des Mitarbeiters hinzufügen'),
            ),
            'inputType' => 'text',
            'eval' => array('tl_class' => 'w50 clr'),
        ),
        'memberTitle' => array(
            'label' => array(
                'de' => array('Position', 'Hier können Sie die Position des Mitarbeiters hinzufügen'),
            ),
            'inputType' => 'text',
            'eval' => array('tl_class' => 'w50'),
        ),
        'contacts' => array(
            'label' => array(
                'de' => array(
                    'Kontakten',
                    'Fügen Sie eine beliebige Anzahl an Kontakten ein.',
                ),
            ),
            'elementLabel' => array(
                'de' => '%s. Social-Media-Profil',
                'en' => '%s. social media profile',
            ),
            'inputType' => 'list',
            'fields' => array(
                'contactsImage' => array(
                    'label' => array(
                        'de' => array('Icon', 'Hier können Sie das Bild auswählen'),
                    ),
                    'inputType' => 'fileTree',
                    'eval' => array(
                        'fieldType' => 'radio',
                        'filesOnly' => true,
                        'extensions' => 'jpg,jpeg,png,gif,svg',
                        'tl_class'=>'w50',
                    ),
                ),
                'url' => array(
                    'label' => array(
                        'de' => array('Kontakt', 'Telefonnumen, Link, Email'),
                    ),
                    'inputType' => 'text',
                    'eval' => array('tl_class' => 'w50'),
                ),
            ),
        ),
        'memberContactsDescription' =>  array(
            'label' => array(
                'de' => array('Beschreibung von den Kontakten'),
                'en' => array('Description'),
            ),
            'inputType' => 'textarea',
            'eval' => array('rte' => 'tinyMCE'),
        ),
        'memberDescription' =>  array(
            'label' => array(
                'de' => array('Beschreibung vom Mitarbeiter/in'),
                'en' => array('Description'),
            ),
            'inputType' => 'textarea',
            'eval' => array('rte' => 'tinyMCE'),
        ),

        'css' => array(
            'label' => array(
                'de' => array('CSS-Klasse', 'Hier können Sie beliebig viele Klassen eingeben'),
            ),
            'inputType' => 'text',
            'eval' => array('tl_class'=>'w50'),
        ),

        'cssImage' => array(
            'label' => array(
                'de' => array('Podsition für Spalte mit Bild', 'Hier können Sie die Position für den Container festlegen'),
            ),
            'inputType' => 'text',
            'eval' => array('tl_class'=>'w50 clr'),
        ),

        'cssInfo' => array(
            'label' => array(
                'de' => array('Position für Spalte mit Info', 'Hier können Sie die Position für den Container festlegen'),
            ),
            'inputType' => 'text',
            'eval' => array('tl_class'=>'w50'),
        ),

    ),
);