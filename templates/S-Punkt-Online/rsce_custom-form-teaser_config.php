<?php
return array(
    'label' => array(
        'de' => array('Anfrage-Formular mit eine Beschreibung', ''),
    ),
    'types' => array('content'),
    'contentCategory' => 'RSCE: Formular',
    //'beTemplate' => 'be_wildcard',
    'standardFields' => array('headline', 'cssID'),
    'wrapper' => array(
        'type' => 'none',
    ),

    'fields' => array(
        'css' => array(
            'label' => array(
                'de' => array('CSS-Klasse', 'Hier können Sie beliebig viele Klassen eingeben'),
            ),
            'inputType' => 'text',
            'eval' => array('tl_class'=>'w50'),
        ),

        'image' => array(
            'label' => array(
                'de' => array('Bild', 'Hier können Sie das Bild auswählen'),
            ),
            'inputType' => 'fileTree',
            'eval' => array(
                'fieldType' => 'radio',
                'filesOnly' => true,
                'extensions' => 'jpg,jpeg,png,gif,svg,webp',
                'tl_class' => 'w50 clr',
            ),
        ),

        'imageAlt' => array(
            'label' => array(
                'de' => array('Alternativer Text', 'Hier können Sie einen alternativen Text für das bild eingeben (alt-Attribut)'),
            ),
            'inputType' => 'text',
            'eval' => array(
                'tl_class' => 'w50 clr',
                'mandatory' => true,
            ),
        ),

        'image_title' => array(
            'label' => array(
                'de' => array('Bildtitel', 'Hier können Sie den Titel des Bildes eingeben (title-Attribut)'),
            ),
            'inputType' => 'text',
            'eval' => array(
                'tl_class' => 'w50',
                'mandatory' => true,
            ),
        ),

        'description' => array(
            'label' => array(
                'de' => array('Beschreibung', 'Hier können Sie die Beschreibung hinzufügen'),
            ),
            'inputType' => 'textarea',
            'eval' => array(
                'tl_class' => 'clr',
                'rte' => 'tinyMCE',
            ),
        ),

        'button_text' => array(
            'label' => array(
                'de' => array('Button Text', 'Hier können Sie einen Text hinzufügen'),
                'en' => array('Link-Text'),
            ),
            'inputType' => 'text',
            'eval' => array('tl_class' => 'w50'),
        ),

        'button_icon' => array(
            'label' => array(
                'de' => array('Icon', 'Hier können Sie das Bild auswählen'),
            ),
            'inputType' => 'fileTree',
            'eval' => array(
                'fieldType' => 'radio',
                'filesOnly' => true,
                'extensions' => 'jpg,jpeg,png,gif,svg,webp',
                'tl_class'=>'w50',
            ),
        ),
    ),
);