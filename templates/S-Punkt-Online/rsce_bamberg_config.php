<?php
#####################################
### Created by S Punkt Online #######
### https://www.s-punkt-online.de ###
#####################################

return array (
    'label' => array( 'Referenzen-Liste: ein Bild, eine Überschrift, ein Text und ein Link' ),
    'types' => array( 'content' ),
    'contentCategory' => 'RSCE: Referenzen',
    'standardFields' => array( 'cssID' ),
    'wrapper' => array(
        'type' => 'none',
    ),

    'fields' => array(
        'list' => array(
            'label' => array('Referenzen - Liste', 'Hier können Sie Referenzen hinzufügen'),
            'elementLabel' => '%s. Referenz Item',
            'inputType' => 'list',
            'minItems' => 1,
            'fields' => array(
                ### group ###
                'hex_group' => array(
                    'label' => array( 'Gradient-Farbe', 'Hier können Sie Gradient hinzufügen.' ),
                    'inputType' => 'group',
                ),

                ### text ###
                'hex' => array(
                    'label' => array(
                        'de' => array('Gradient-Farbe von:', 'Hier können Sie HEX eingeben'),
                    ),
                    'inputType' => 'text',
                    'eval' => array(
                        'tl_class' => 'w50 clr',
                        //'mandatory' => true
                    ),
                ),

                ### text ###
                'hex_1' => array(
                    'label' => array(
                        'de' => array('Gradient-Farbe bis:', 'Hier können Sie HEX eingeben'),
                    ),
                    'inputType' => 'text',
                    'eval' => array(
                        'tl_class' => 'w50',
                        //'mandatory' => true
                    ),
                ),
                ### group ###
                'image_group' => array(
                    'label' => array( 'Bild', 'Hier können Sie ein Bild hinzufügen.' ),
                    'inputType' => 'group',
                ),

                ### image ###
                'image' => array(
                    'label' => array(
                        'de' => array('Bild', 'Hier können Sie das Bild auswählen'),
                    ),
                    'inputType' => 'fileTree',
                    'eval' => array(
                        'fieldType' => 'radio',
                        'filesOnly' => true,
                        'extensions' => 'jpg,jpeg,png,gif,svg',
                        'tl_class'=>'clr',
                        'mandatory' => true,
                    ),
                ),

                ### image alt ###
                'image_alt' => array(
                    'label' => array(
                        'de' => array('Alt-Attribute', 'Hier können Sie einen Alt-Attribute für das Bild eingeben'),
                    ),
                    'inputType' => 'text',
                    'eval' => array(
                        'tl_class' => 'w50 clr',
                        'mandatory' => true,
                    ),
                ),

                ### image title ###
                'image_title' => array(
                    'label' => array(
                        'de' => array('Bildtitel', 'Hier können Sie den Titel des Bildes eingeben (title-Attribut)'),
                    ),
                    'inputType' => 'text',
                    'eval' => array(
                        'tl_class' => 'w50 clr',
                        'mandatory' => true,
                    ),
                ),
                ### group ###
                'text_group' => array(
                    'label' => array( 'Die Beschreibung', 'Hier können Sie eine Überschrift und den Text hinzufügen.' ),
                    'inputType' => 'group',
                ),

                ### subtitle ###
                'title' => array(
                    'label' => array(
                        'de' => array('Überschrift', 'Hier können Sie die Überschrift einfügen'),
                    ),
                    'inputType' => 'inputUnit',
                    'options' => array('h3', 'h4', 'h5', 'h6'),
                    'eval' => array('tl_class' => 'w50 clr'),
                ),

                ### text ###
                'text' => array(
                    'label' => array(
                        'de' => array('Text', 'Hier können Sie den Text einfügen'),
                    ),
                    'inputType' => 'textarea',
                    'eval' => array(
                        'tl_class' => 'w50',
                    ),
                ),

                ### group ###
                'link_group' => array(
                    'label' => array( 'Der Link', 'Hier können Sie einen Link hinzufügen.' ),
                    'inputType' => 'group',
                ),

                ### link ###
                'link' => array(
                    'label' => array(
                        'de' => array('Link', 'Hier können Sie das Link auswählen'),
                        'en' => array('Link'),
                    ),
                    'inputType' => 'url',
                    'eval' => array(
                        'tl_class' => 'w50 clr',
                        'mandatory' => true
                    ),
                ),

                // IMAGE;
                'link_icon' => array(
                    'label' => array(
                        'de' => array('Icon', 'Hier können Sie das Bild auswählen'),
                    ),
                    'inputType' => 'fileTree',
                    'eval' => array(
                        'fieldType' => 'radio',
                        'filesOnly' => true,
                        'extensions' => 'jpg,jpeg,png,gif,svg',
                        'tl_class'=>'w50',
                        'mandatory' => true
                    ),
                ),

                // TEXT;
                'link_text' => array(
                    'label' => array(
                        'de' => array('Text', 'Hier können Sie einen Text hinzufügen'),
                        'en' => array('Link-Text'),
                    ),
                    'inputType' => 'text',
                    'eval' => array(
                        'tl_class' => 'w50',
                        'mandatory' => true
                    ),
                ),

                // TEXT;
                'link_title' => array(
                    'label' => array(
                        'de' => array('Link-Title', 'Hier können Sie einen Text hinzufügen'),
                        'en' => array('Link-Title'),
                    ),
                    'inputType' => 'text',
                    'eval' => array(
                        'tl_class' => 'w50',
                        'mandatory' => true
                    ),
                ),
            ),
        ),
    ),
);