<?php
return array(
    'label' => array(
        'de' => array('Referenzen-Preview:  Hintergrund, Icon, Link, Titel, Beschreibung', ''),
    ),
    'types' => array('content'),
    'contentCategory' => 'RSCE: Inhalt',
    //'beTemplate' => 'be_wildcard',
    'standardFields' => array('cssID'),
    'wrapper' => array(
        'type' => 'none',
    ),

    'fields' => array(
        'gallery' => array(
            'label' => array(
                'de' => array(
                    'Die Liste',
                    'Fügen Sie eine beliebige Anzahl an Bilder mit einem Text und Link, Bild und Logo ein.',
                ),
            ),
            'elementLabel' => array(
                'de' => 'Item %s',
            ),
            'inputType' => 'list',
            'fields' => array(
                'galleryImage' => array(
                    'label' => array(
                        'de' => array('Hintergrundbild', 'Hier können Sie das Bild auswählen'),
                    ),
                    'inputType' => 'fileTree',
                    'eval' => array(
                        'fieldType' => 'radio',
                        'filesOnly' => true,
                        'extensions' => 'jpg,jpeg,png,gif,svg',
                        'tl_class'=>'w50',
                    ),
                ),
                'galleryImageAlt' => array(
                    'label' => array(
                        'de' => array('Alternativer Text', 'Hier können Sie einen alternativen Text für das bild eingeben (alt-Attribut)'),
                    ),
                    'inputType' => 'text',
                    'eval' => array(
                        'tl_class' => 'w50 clr',
                        'mandatory' => true,
                    ),
                ),
                'galleryIcon' => array(
                    'label' => array(
                        'de' => array('Logo', 'Hier können Sie das Bild auswählen'),
                    ),
                    'inputType' => 'fileTree',
                    'eval' => array(
                        'fieldType' => 'radio',
                        'filesOnly' => true,
                        'extensions' => 'jpg,jpeg,png,gif,svg',
                        'tl_class'=>'w50 clr',
                    ),
                ),
                'galleryIconAlt' => array(
                    'label' => array(
                        'de' => array('Alternativer Text', 'Hier können Sie einen alternativen Text für das bild eingeben (alt-Attribut)'),
                    ),
                    'inputType' => 'text',
                    'eval' => array(
                        'tl_class' => 'w50 clr',
                        'mandatory' => true,
                    ),
                ),
                'galleryTitle' => array(
                    'label' => array(
                        'de' => array('Projekt-Name', 'Hier können Sie Projekt-Name hinzufügen'),
                    ),
                    'inputType' => 'text',
                    'eval' => array(
                        'tl_class' => 'w50 clr',
                    ),
                ),
                'galleryDescription' => array(
                    'label' => array(
                        'de' => array('Kundenname', 'Hier können Sie Kundenname hinzufügen'),
                    ),
                    'inputType' => 'text',
                    'eval' => array(
                        'tl_class' => 'w50',
                    ),
                ),
                'galleryLink' => array(
                    'label' => array(
                        'de' => array('Link', 'Hier können Sie ein Link auswählen'),
                    ),
                    'inputType' => 'url',
                    'eval' => array('tl_class' => 'w50 clr'),
                ),
                'galleryLinkIcon' => array(
                    'label' => array(
                        'de' => array('Icon', 'Hier können Sie das Bild auswählen'),
                    ),
                    'inputType' => 'fileTree',
                    'eval' => array(
                        'fieldType' => 'radio',
                        'filesOnly' => true,
                        'extensions' => 'jpg,jpeg,png,gif,svg',
                        'tl_class'=>'w50',
                    ),
                ),
                'galleryLinkText' => array(
                    'label' => array(
                        'de' => array('Link-Text', 'Hier können Sie den Text für Button hinzufügen'),
                    ),
                    'inputType' => 'text',
                    'eval' => array('tl_class' => 'w50'),
                ),
                'galleryLinkTitle' => array(
                    'label' => array(
                        'de' => array('Link-Title', 'Hier können Sie den Text für Button hinzufügen'),
                    ),
                    'inputType' => 'text',
                    'eval' => array('tl_class' => 'w50'),
                ),
            ),
        ),
    ),
);