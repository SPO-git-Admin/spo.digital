<?php
return array(
    'label' => array(
        'de' => array('Footer: Partner-Logos und Social-Media Icons', 'Eine strukturierte Vorlage für Footer Icons'),
    ),

    'types' => array('content'),
    'contentCategory' => 'RSCE: NODE',
    //'beTemplate' => 'be_wildcard',
    'standardFields' => array('cssID'),
    'wrapper' => array(
        'type' => 'none',
    ),
    
    'fields' => array(
        'partners_list' => array(
            'label' => array(
                'de' => array('Liste der  Partner-Logos', 'Hier können Sie Partner-Logo ergänzen'),
            ),
            'elementLabel' => array(
                'de' => 'Partner-Logo %s',
            ),

            'inputType' => 'list',
            'fields' => array(
                'logo' => array(
                    'label' => array(
                        'de' => array('Logo', 'Hier können Sie ein Logo auswählen'),
                    ),
                    'inputType' => 'fileTree',
                    'eval' => array(
                        'fieldType' => 'radio',
                        'filesOnly' => true,
                        'extensions' => 'jpg,jpeg,png,gif,svg',
                        'mandatory' => true,
                    ),
                ),
                'alt' => array(
                    'label' => array(
                        'de' => array('Alt-Attribute', 'Hier können Sie Alt-Attribute für das Logo hinzufügen'),
                    ),
                    'inputType' => 'text',
                    'eval' => array('tl_class' => 'w50', 'mandatory' => true),
                ),
                'link' => array(
                    'label' => array(
                        'de' => array('Link', 'Hier können Sie das Link auswählen'),
                    ),
                    'inputType' => 'url',
                    'eval' => array('tl_class' => 'w50 clr', 'mandatory' => true),
                ),
                'aria_label' => array(
                    'label' => array(
                        'de' => array('Aria-label', 'Hier können Sie Aria-Label hinzufügen'),
                    ),
                    'inputType' => 'text',
                    'eval' => array('tl_class' => 'w50', 'mandatory' => true),
                ),
            ),
        ),
        'description' => array(
            'label' => array(
                'de' => array('Beschreibung', 'Hier können Sie die Beschreibung hinzufügen'),
            ),
            'inputType' => 'textarea',
            'eval' => array(
                'tl_class' => 'clr',
                'rte' => 'tinyMCE',
            ),
        ),
    ),
);