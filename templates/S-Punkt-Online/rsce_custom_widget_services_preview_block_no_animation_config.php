<?php

#####################################
### Created by S Punkt Online #######
### https://www.s-punkt-online.de ###
#####################################

return array(
    'label' => array(
        'de' => array('Leistungen-Preview: Eine strukturierte Vorlage für Leistungen-Preview', ''),
    ),

    'types' => array('content'),
    'contentCategory' => 'RSCE: Inhalt',
    //'beTemplate' => 'be_wildcard',
    'standardFields' => array('cssID'),
    'wrapper' => array(
        'type' => 'none',
    ),
    


    'fields' => array(
        'groupList' => array(
            'label' => array(
                'de' => array(
                    'Leistungen',
                    'Fügen Sie eine beliebige Anzahl an Icons mit einem Text und Link ein.',
                ),
            ),
            'elementLabel' => array(
                'de' => 'Item %s',
            ),
            'inputType' => 'list',
            'fields' => array(

                'image' => array(
                    'label' => array(
                        'de' => array('Bild', 'Hier können Sie das Bild auswählen'),
                    ),
                    'inputType' => 'fileTree',
                    'eval' => array(
                        'fieldType' => 'radio',
                        'filesOnly' => true,
                        'extensions' => 'jpg,jpeg,png,gif,svg',
                        'tl_class'=>'w50',
                    ),
                ),

                // TEXT
                'image_alt' => array(
                    'label' => array(
                        'de' => array('Alt-Attribute', 'Hier können Sie einen Alt-Attribute für das Bild eingeben'),
                    ),
                    'inputType' => 'text',
                    'eval' => array(
                        'tl_class' => 'w50 clr',
                        'mandatory' => true,
                    ),
                ),

                // TEXT
                'image_title' => array(
                    'label' => array(
                        'de' => array('Bildtitel', 'Hier können Sie den Titel des Bildes eingeben (title-Attribut)'),
                    ),
                    'inputType' => 'text',
                    'eval' => array('tl_class' => 'w50'),
                ),

                'title' => array(
                    'label' => array(
                        'de' => array('Item Überschrift', 'Hier können Sie einen Überschrift einfügen'),
                    ),
                    'inputType' => 'text',
                    'eval' => array('tl_class'=>'w50 clr'),
                ),

                'description' => array(
                    'label' => array(
                        'de' => array('Item Beschreibung', 'Hier können Sie einen Text einfügen'),
                        'en' => array('Item Description', 'Here you can insert a text'),
                    ),
                    'inputType' => 'textarea',
                    'eval' => array('rte' => 'tinyMCE', 'tl_class' => 'w50 clr'),
                ),

                'link' => array(
                    'label' => array(
                        'de' => array('Button-Link', 'Hier können Sie einen Link auswählen'),
                    ),
                    'inputType' => 'url',
                    'eval' => array('tl_class' => 'w50 clr'),
                ),
                'link_icon' => array(
                    'label' => array(
                        'de' => array('Icon', 'Hier können Sie das Bild auswählen'),
                    ),
                    'inputType' => 'fileTree',
                    'eval' => array(
                        'fieldType' => 'radio',
                        'filesOnly' => true,
                        'extensions' => 'jpg,jpeg,png,gif,svg',
                        'tl_class'=>'w50',
                    ),
                ),
                'link_text' => array(
                    'label' => array(
                        'de' => array('Link-Text', 'Hier können Sie einen Text für den Link eingeben'),
                    ),
                    'inputType' => 'text',
                    'eval' => array('tl_class' => 'w50 clr'),
                ),
                'link_title' => array(
                    'label' => array(
                        'de' => array('Link-Title', 'Hier können Sie einen Titel für den Link eingeben'),
                    ),
                    'inputType' => 'text',
                    'eval' => array('tl_class' => 'w50 clr'),
                ),
            ),
        ),
    ),
);